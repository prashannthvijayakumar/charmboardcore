# CharmboardCore

[![CI Status](http://img.shields.io/travis/prashannth/CharmboardCore.svg?style=flat)](https://travis-ci.org/prashannth/CharmboardCore)
[![Version](https://img.shields.io/cocoapods/v/CharmboardCore.svg?style=flat)](http://cocoapods.org/pods/CharmboardCore)
[![License](https://img.shields.io/cocoapods/l/CharmboardCore.svg?style=flat)](http://cocoapods.org/pods/CharmboardCore)
[![Platform](https://img.shields.io/cocoapods/p/CharmboardCore.svg?style=flat)](http://cocoapods.org/pods/CharmboardCore)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CharmboardCore is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CharmboardCore'
```

## Author

prashannth, v@prashannth.com

## License

CharmboardCore is available under the MIT license. See the LICENSE file for more info.
