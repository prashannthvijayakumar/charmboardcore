//
//  Charm.m
//  KPCHSdk
//
//  Created by apple on 17/01/17.
//  Copyright © 2017 apple. All rights reserved.
//
#import "Charm.h"
@implementation Charm

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (instancetype)initWithJSONString:(NSDictionary *)JSONDictionary
{
    self = [super init];
    if (self) {
            //Loop method
            for (NSString* key in JSONDictionary) {
                @try {
                    [self setValue:[JSONDictionary valueForKey:key] forKey:key];
                } @catch (NSException *exception) {
                    continue;
                } @finally {
                    
                }
            }
        self.title = [self getString:self.title];
        self.subtitle = [self getString:self.subtitle];
        
    }
    return self;
}
-(NSString *) getString : (NSString *)st{
    st = [st stringByReplacingOccurrencesOfString:@"-" withString:@" "];
    st = [st capitalizedString];
    return st;
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.id forKey:@"id"];
    [aCoder encodeObject:self.title forKey:@"title"];
    [aCoder encodeObject:self.subtitle forKey:@"subtitle"];
    [aCoder encodeObject:self.objid forKey:@"objid"];
    [aCoder encodeObject:self.seek forKey:@"seek"];
    [aCoder encodeObject:self.haslook forKey:@"haslook"];
    [aCoder encodeObject:self.lcbox forKey:@"lcbox"];
    [aCoder encodeObject:self.vid forKey:@"vid"];
    [aCoder encodeObject:self.vtitle forKey:@"vtitle"];

}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super init]){
        self.id = [aDecoder decodeObjectForKey:@"id"];
        self.title = [aDecoder decodeObjectForKey:@"title"];
        self.subtitle = [aDecoder decodeObjectForKey:@"subtitle"];
        self.objid = [aDecoder decodeObjectForKey:@"objid"];
        self.seek = [aDecoder decodeObjectForKey:@"seek"];
        self.haslook = [aDecoder decodeObjectForKey:@"haslook"];
        self.lcbox = [aDecoder decodeObjectForKey:@"lcbox"];
        self.vid = [aDecoder decodeObjectForKey:@"vid"];
        self.vtitle = [aDecoder decodeObjectForKey:@"vtitle"];
    }
    return self;
}

@end
