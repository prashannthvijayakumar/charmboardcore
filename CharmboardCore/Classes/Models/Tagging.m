//
//  Tagging.m
//  Charmboard
//
//  Created by Prashannth Vijayakumar on 9/29/17.
//

#import "Tagging.h"

@implementation Tagging

- (id) init {
    self = [super init];
    if(self){
    }
    return self;
}

-(instancetype)initWithStartX:(int)startX startY:(int)startY endX:(int)endX endY:(int)endY objectID:(int)objectID
{
    self = [super init];
    if (self) {
        self.startX = startX;
        self.startY = startY;
        self.endX = endX;
        self.endY = endY;
        self.objectID = objectID;
    }
    return self;
}

- (int)getEndX{
    return self.endX;
}
- (int)getEndY{
    return self.endY;
}
- (int)getObjectID{
    return self.objectID;
}
- (int)getStartX{
    return self.startX;
}
- (int)getStartY{
    return self.startY;
}

@end
