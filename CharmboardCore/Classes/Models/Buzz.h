//
//  Buzz.h
//  JWTouch
//
//  Created by Prashannth Vijayakumar on 07/11/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface Buzz : NSObject
@property (nonatomic) NSString *id;
@property (nonatomic) NSString *datetime;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *content;
@property (nonatomic) NSString *unread;
@property (nonatomic) NSString *starred;
@property (nonatomic) NSString *source;
@property (nonatomic) NSString *thumbnail;
@property (nonatomic) NSString *icon;
@property (nonatomic) NSString *uid;
@property (nonatomic) NSString *link;
@property (nonatomic) NSString *updatetime;
@property (nonatomic) NSString *author;
@property (nonatomic) NSString *sourcetitle;
@property (nonatomic) NSString *tags;

@property (nonatomic) NSString *sourceTag;

-(instancetype)initWithJSONString:(NSDictionary *)JSONString sourceTag:(NSString *)sourceTag;

-(NSString *)getDateTime;
-(NSString *)getTitle;
-(NSString *)getContent;
-(NSString *)getThumbnail;
@end
