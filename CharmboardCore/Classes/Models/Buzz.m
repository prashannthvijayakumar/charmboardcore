//
//  Buzz.m
//  JWTouch
//
//  Created by Prashannth Vijayakumar on 07/11/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "Buzz.h"
@interface Buzz()
@property(nonatomic, strong) NSString *titleStr;
@property(nonatomic, strong) NSString *retweetStr;
@property(nonatomic, strong) NSString *contentStr;
@property(nonatomic, strong) NSString *thumbnailStr;
@property(nonatomic, strong) NSDate *date;
@end

@implementation Buzz

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (instancetype)initWithJSONString:(NSDictionary *)JSONDictionary sourceTag:(NSString *)sourceTag
{
    self = [super init];
    if (self) {
        self.sourceTag = sourceTag;
        //Loop method
        for (NSString* key in JSONDictionary) {
            @try {
                [self setValue:[JSONDictionary valueForKey:key] forKey:key];
            } @catch (NSException *exception) {
                continue;
            } @finally {
                
            }
        }
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        self.date = [dateFormat dateFromString: self.datetime];

        if([self.sourceTag isEqualToString:@"twitter"]){
            self.retweetStr = @"";
            NSMutableArray *buzzTitleArray = [(NSArray*)[self.title componentsSeparatedByString:@":<br />"] mutableCopy];
            self.titleStr = [buzzTitleArray objectAtIndex:0];
            [buzzTitleArray removeObjectAtIndex:0];
            self.contentStr = [buzzTitleArray componentsJoinedByString:@" "];
            
            NSMutableArray *contentArray = [(NSArray*)[self.contentStr componentsSeparatedByString:@"<a href"] mutableCopy];
            self.contentStr = [contentArray objectAtIndex:0];
            
            NSMutableArray *titleArray = [(NSArray*)[self.titleStr componentsSeparatedByString:@"(RT "] mutableCopy];
            self.titleStr = [titleArray objectAtIndex:0];
            if([titleArray count]>1){
                self.retweetStr = [titleArray[1] stringByReplacingOccurrencesOfString:@")" withString:@""];
            }
        }else if([self.sourceTag isEqualToString:@"fb"]){
            self.titleStr = self.sourcetitle;
            if ([self.titleStr rangeOfString:@"Facebook-"].location != NSNotFound) {
                NSString *original = @"Facebook-";
                NSString *replacement = @"";
                NSRange rOriginal = [self.titleStr rangeOfString:original];
                if (NSNotFound != rOriginal.location) {
                    self.titleStr = [self.titleStr stringByReplacingCharactersInRange:rOriginal withString:replacement];
                }
            } else if([self.titleStr rangeOfString:@"Fb-"].location != NSNotFound){
                NSString *original = @"Fb-";
                NSString *replacement = @"";
                NSRange rOriginal = [self.titleStr rangeOfString:original];
                if (NSNotFound != rOriginal.location) {
                    self.titleStr = [self.titleStr stringByReplacingCharactersInRange:rOriginal withString:replacement];
                }
            }
            
            NSMutableArray *contentArray = [(NSArray*)[self.title componentsSeparatedByString:@"-"] mutableCopy];
            if([contentArray count] > 0){
                [contentArray removeObjectAtIndex:0];
                self.contentStr = [contentArray componentsJoinedByString:@" "];
            }else{
                self.contentStr = [contentArray objectAtIndex:0];
            }
       
            self.thumbnailStr=@"";
            NSArray* thumbArray = [self.content componentsSeparatedByString: @"<img src=\""];
            if([thumbArray count]>1){
                self.thumbnailStr = [thumbArray objectAtIndex:1];
                thumbArray = [self.thumbnailStr componentsSeparatedByString:@"\""];
                self.thumbnailStr = [thumbArray objectAtIndex:0];
                self.thumbnailStr = [self.thumbnailStr stringByReplacingOccurrencesOfString:@"&amp;"
                                                                   withString:@"&"];
            }
        }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.id forKey:@"id"];
    [aCoder encodeObject:self.datetime forKey:@"datetime"];
    [aCoder encodeObject:self.title forKey:@"title"];
    [aCoder encodeObject:self.content forKey:@"content"];
    [aCoder encodeObject:self.unread forKey:@"unread"];
    [aCoder encodeObject:self.starred forKey:@"starred"];
    [aCoder encodeObject:self.source forKey:@"source"];
    [aCoder encodeObject:self.thumbnail forKey:@"thumbnail"];
    [aCoder encodeObject:self.icon forKey:@"icon"];
    [aCoder encodeObject:self.uid forKey:@"uid"];
    [aCoder encodeObject:self.link forKey:@"link"];
    [aCoder encodeObject:self.updatetime forKey:@"updatetime"];
    [aCoder encodeObject:self.author forKey:@"author"];
    [aCoder encodeObject:self.sourcetitle forKey:@"sourcetitle"];
    [aCoder encodeObject:self.tags forKey:@"tags"];

}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super init]){
        self.id = [aDecoder decodeObjectForKey:@"id"];
        self.datetime = [aDecoder decodeObjectForKey:@"datetime"];
        self.title = [aDecoder decodeObjectForKey:@"title"];
        self.content = [aDecoder decodeObjectForKey:@"content"];
        self.unread = [aDecoder decodeObjectForKey:@"unread"];
        self.starred = [aDecoder decodeObjectForKey:@"starred"];
        self.source = [aDecoder decodeObjectForKey:@"source"];
        self.thumbnail = [aDecoder decodeObjectForKey:@"thumbnail"];
        self.icon = [aDecoder decodeObjectForKey:@"icon"];
        self.uid = [aDecoder decodeObjectForKey:@"uid"];
        self.link = [aDecoder decodeObjectForKey:@"link"];
        self.updatetime = [aDecoder decodeObjectForKey:@"updatetime"];
        self.author = [aDecoder decodeObjectForKey:@"author"];
        self.sourcetitle = [aDecoder decodeObjectForKey:@"sourcetitle"];
        self.tags = [aDecoder decodeObjectForKey:@"tags"];
    }
    return self;
}

-(NSString *)getDateTime{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMMM d, yyyy h:mm a"];
    NSString *dateStr = [dateFormat stringFromDate:self.date];
    return dateStr;

}

-(NSString *)getTitle{
    if([self.sourceTag isEqualToString:@"twitter"]){ //Twitter
        return self.titleStr;
    } else if ([self.sourceTag isEqualToString:@"ig"]){ //Instagram
        return self.author;
    } else if ([self.sourceTag isEqualToString:@"fb"]){ //Facebook
        return self.titleStr;
    }
    return self.title;
}

-(NSString *)getContent{
    if([self.sourceTag isEqualToString:@"twitter"]){
        if([self.retweetStr length] > 0){
            return [NSString stringWithFormat:@"<a href=\"%@\" target=\"_blank\"> %@ retweeted</a> <br /> %@",self.link,self.retweetStr,self.contentStr];
        }
        return self.contentStr;
    } else if ([self.sourceTag isEqualToString:@"ig"]){
        if([self.title isEqualToString:@"[no title]"]){
            return @"";
        }
        return self.title;
    } else if ([self.sourceTag isEqualToString:@"fb"]){
        return self.contentStr;
    }
    return self.content;
}

-(NSString *)getThumbnail{
    if([self.sourceTag isEqualToString:@"twitter"] && [self.thumbnail length] > 0){
        return [NSString stringWithFormat:@"https://ap.charmboard.com/thumbnails/%@",self.thumbnail];
    } else if ([self.sourceTag isEqualToString:@"ig"] && [self.link length] > 0){
        return [NSString stringWithFormat:@"%@%@",self.link,@"media/?size=m"];
    } else if ([self.sourceTag isEqualToString:@"fb"]){
        return self.thumbnailStr;
    }
    return self.thumbnail;
}

@end
