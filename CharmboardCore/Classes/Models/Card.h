//
//  Card.h
//  KPCHSdk
//
//  Created by apple on 18/01/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Card : NSObject
@property (nonatomic) NSString *touch_id;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *subtitle;
@property (nonatomic) NSString *brand;
@property (nonatomic) NSString *moreimages;
@property (nonatomic) NSString *dominant_color;
@property (nonatomic) NSString *domain;
- (instancetype)initWithJSONString:(NSDictionary *)JSONString;
@end
