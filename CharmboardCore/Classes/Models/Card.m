//
//  Card.m
//  KPCHSdk
//
//  Created by apple on 18/01/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "Card.h"

@implementation Card
- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}
- (instancetype)initWithJSONString:(NSDictionary *)JSONDictionary
{
    self = [super init];
    if (self) {
        //Loop method
        for (NSString* key in JSONDictionary) {
            @try {
                [self setValue:[JSONDictionary valueForKey:key] forKey:key];
            } @catch (NSException *exception) {
                continue;
            } @finally {
                
            }
        }
        self.domain = [self getDomain:self.brand];
    }
    return self;
}

-(NSString *) getDomain:(NSString *) link{
    if([ link hasPrefix:@"undefined"] && [link  isEqual: @""]){
        return @"";
    }
    NSString *urlString = link;
    NSURL *url = [NSURL URLWithString:urlString];
    NSString *d_domain = url.host;
    NSArray *d_array = [d_domain componentsSeparatedByString:@"."];
    if([d_domain hasPrefix:@"www"]){
        return [d_array[1] capitalizedString];
    }
    return [d_array[0] capitalizedString];
}
@end
