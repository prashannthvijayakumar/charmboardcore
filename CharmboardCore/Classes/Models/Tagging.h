//
//  Tagging.h
//  Charmboard
//
//  Created by Prashannth Vijayakumar on 9/29/17.
//

#import <Foundation/Foundation.h>

@interface Tagging : NSObject

@property(nonatomic) int startX;
@property(nonatomic) int endX;
@property(nonatomic) int startY;
@property(nonatomic) int endY;
@property(nonatomic) int objectID;

- (int)getEndX;
- (int)getEndY;
- (int)getObjectID;
- (int)getStartX;
- (int)getStartY;

-(instancetype)initWithStartX:(int)startX startY:(int)startY endX:(int)endX endY:(int)endY objectID:(int)objectID;

@end
