//
//  Charm.h
//  KPCHSdk
//
//  Created by apple on 17/01/17.
//  Copyright © 2017 apple. All rights reserved.
//
#import <Foundation/Foundation.h>
@interface Charm : NSObject
@property (nonatomic) NSString *id;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *subtitle;
@property (nonatomic) NSString *objid;
@property (nonatomic) NSString *seek;
@property (nonatomic) NSString *haslook;
@property (nonatomic) NSMutableArray *lcbox;
@property (nonatomic) NSString *vid;
@property (nonatomic) NSString *vtitle;

- (instancetype)initWithJSONString:(NSDictionary *)JSONString;
@end
