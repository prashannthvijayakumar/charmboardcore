//
//  Constants.m
//  JWTouch
//
//  Created by Prashannth Vijayakumar on 14/11/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "Constants.h"

@implementation Constants

//URL
NSString *const STRING_GET_VIDEO_OBJECT_ID = @"https://bi.charmboard.com/video/vid_%@?x=%f&y=%f&inputframe=%d&fps=25";
NSString *const STRING_GET_JSON = @"https://cbstatic.tagos.in/to/%@_tag_v2.json";
NSString *const STRING_GET_TOPCHARMS = @"https://api.charmboard.com/v1/cards/topcharms/%@";
NSString *const STRING_GET_VIDEO_TAGGED = @"https://api.charmboard.com/v1/ive/%@";
NSString *const STRING_GET_CARD_OUTFITS = @"https://api.charmboard.com/v1/cards/look/outfits/%@";
NSString *const STRING_GET_CARD_ACCESSORIES = @"https://api.charmboard.com/v1/cards/look/accessories/%@";
NSString *const STRING_GET_CARD_LOCATION = @"http://dev.charmboard.com:8083/v1/cards/scene/location/%@";
NSString *const STRING_GET_SKETCH = @"http://dev.charmboard.com:8083/v1/sktid/%@";
NSString *const STRING_GET_SUBMIT_UNCREDITED = @"https://api.charmboard.com/v1/sendmail?text=%@&from=%@&to=feedback@charmboard.com&subject=Actor%%20information%%20for%%20charmid%%20%@";
NSString *const STRING_GET_CAST = @"http://dev.charmboard.com:8083/v1/cards/cast/%@";
NSString *const STRING_GET_BUZZ = @"https://ap.charmboard.com/items?tag=%@_%@&type=newest&updatedsince=1&items=4";
NSString *const STRING_CHARM_SHARE = @"https://www.charmboard.com/en/style-watch/%@/%@/style-of-%@-in-serial.html #BeInspired";


//Image
NSString *const STRING_IMAGE_PLACEHOLDER = @"nointernet.png";
NSString *const STRING_IMAGE_ERROR = @"tagos_broken.png";
NSString *const STRING_IMAGE_LEAD_CHARM = @"https://cbstatic.tagos.in/im/lc/%@.jpg";
NSString *const STRING_IMAGE_LOOK = @"https://cbstatic.tagos.in/im/lk/%@.jpg";
NSString *const STRING_IMAGE_LOCATION = @"https://cbstatic.tagos.in/im/lo/%@.jpg";
NSString *const STRING_IMAGE_LEAD_CHARM_SLIDER1 = @"https://s3-ap-southeast-1.amazonaws.com/charmboard/images/similar/%@/%@/1.jpg";
NSString *const STRING_IMAGE_LEAD_CHARM_SLIDER2 = @"https://s3-ap-southeast-1.amazonaws.com/charmboard/images/similar/%@/%@/2.jpg";
NSString *const STRING_VIDEO_LEAD_CHARM_SLIDER = @"https://s3-ap-southeast-1.amazonaws.com/charmboard/images/similar/%@/%@/4.mp4";
NSString *const STRING_IMAGE_CAST = @"https://cbstatic.tagos.in/im/ca/\%@.jpg";
NSString *const STRING_IMAGE_BUZZ_FAVICON = @"https://ap.charmboard.com/favicons/%@";
NSString *const STRING_IMAGE_CHARM_CARD = @"https://res-2.cloudinary.com/charmboard/image/fetch/w_%d,h_%d/x_%d,y_%d,w_%d,h_%d,c_crop,q_auto:best/w_%d/http://cbstatic.tagos.in/im/lc/%@.jpg";


//PiwikTracker
NSString *const STRING_PIWIK_SERVER_URL = @"https://analytics.charmboard.com/";
NSString *const STRING_PIWIK_SITE_ID = @"3";


//Google Analytics
NSString *const STRING_GA_KEY = @"UA-108424618-1";


//Miscellaneous
NSString *const ARRAY_TAB_SECTIONS_HASLOOK_ALL[] = { @"ALL",@"OUTFIT",@"ACCESSORIES",@"LOCATION",@"ARTIST",@"BUZZ" };
NSString *const ARRAY_TAB_SECTIONS_HASLOOK_RESTRICTED[] = { @"ARTIST",@"BUZZ" };
NSString *const STRING_NO_CARDS = @"No recent cards are available at this time";
NSString *const STRING_INTRO_TITLE = @"While the video plays";
NSString *const STRING_INTRO_SUBTITLE1 = @"Touch the views you love";
NSString *const STRING_INTRO_SUBTITLE2 = @"Discover more here";
NSString *const STRING_INTRO_TOOLTIP = @"Touch here for player controls";
NSString *const STRING_NO_CHARM_ADDED = @"You have not added any Charm. Play the video and click or touch the image that inspires you";
NSString *const STRING_NO_TOP_CHARM = @"No charms are available at this time";
NSString *const ARRAY_TAB_SECTIONS_CHARM[] = { @"YOUR CHARMBOARD", @"TOP CHARMS" };

@end
