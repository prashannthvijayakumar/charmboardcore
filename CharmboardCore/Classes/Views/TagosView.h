//
//  TagosView.h
//  JWTouch
//
//  Created by Prashannth Vijayakumar on 09/11/17.
//  Copyright © 2017 apple. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface TagosView : UIView

@property(nonatomic, strong)UIView *containerView;
@property(nonatomic, strong)UIView *controlView;

-(instancetype)initWithContainerView:(UIView *)containerView controlView:(UIView *)controlView;
-(void)showTooltip;
-(void)hideTooltip;
-(void)showIntro;
-(void)hideIntro;

@end
