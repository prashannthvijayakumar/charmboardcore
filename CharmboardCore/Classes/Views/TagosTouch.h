//
//  TagosTouch.h
//  Charmboard
//
//  Created by Prashannth Vijayakumar on 9/27/17.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Charm.h"
#import "CharmsViewController.h"
#import "TagosView.h"

@interface TagosTouch: NSObject <UIGestureRecognizerDelegate, CharmsViewDelegate>

@property(nonatomic, strong) UIView *tview;
@property(nonatomic, strong) NSString *p_image;
@property(nonatomic, strong) NSString *m_image;
@property(nonatomic, strong) NSString *h_image;
@property(nonatomic, strong) NSMutableDictionary *someDict;
@property(nonatomic, strong) NSMutableDictionary *ObjectDict;
@property(nonatomic, strong) NSMutableArray *charms;
@property(nonatomic, strong) NSMutableArray *myCharms;
@property(nonatomic, strong) UITextView *textView;
@property(nonatomic, strong) UIViewController *videoViewC;
@property(nonatomic, strong) NSMutableDictionary *searchObjectID;
@property(nonatomic, strong) UIImageView *imageView;
@property(nonatomic, strong) UIImageView *controlView;
@property(nonatomic, strong) TagosView *introView;
@property(nonatomic, strong) NSString *videoId;
@property(nonatomic, strong) NSMutableArray *loadedVideos;
@property(nonatomic) BOOL isAdPlaying;
@property(nonatomic) BOOL isFullScreen;
@property(nonatomic) BOOL localjson;
@property(nonatomic) BOOL isCurrentVideoTagged;
@property(nonatomic) BOOL resumeFullScreen;

-(void) loadFloatingButton:(CGFloat *)xCorr withYCorr:(CGFloat *)yCorr withText:(int)text;
-(void) handleTapGesture:(UITapGestureRecognizer *)  gestureRecognizer;
-(double) getCurrentTime;
-(void) loadJson;
-(void) loadData;
-(int)  getObject:(CGFloat *)x withY:(CGFloat *)y withTime:(int)time;
-(void) addSubviewInPlayer:(UIView *)imageview;
-(void) initOverlay;
-(void) play;
-(void) pause;
-(void) resume;
-(void) exitFullScreen;
-(void) enterFullScreen;
-(void) controlSwitch;
-(void) addTagosIntro;
-(void) addTagosui;
-(void) tapDetected:(UITapGestureRecognizer *)  gestureRecognizer;
-(void) addMessage: (NSString *)mtitle withMsubtitle:(NSString *)msubtitle;
-(void) loadMessage: (NSString *)msg;
-(void) changeTextView;
-(void) touchToFirebase: (Charm *)charm;
-(void) controlTapDetected:(UITapGestureRecognizer *)  gestureRecognizer;
-(void) saveCharms;
-(void) loadCharms;
-(void) isVideoTagged : (void(^)(BOOL ive))completionHandler;
-(void) hideOverlay;
-(void) showOverlay;
-(void) showOverlayIfVideoTagged;
-(void) updateVideoId:(NSString *) videoId;
-(BOOL) isVideoPlaying;

-(instancetype) initWithController:(UIViewController *)videoViewC videoId:(NSString *)videoId;

@end
