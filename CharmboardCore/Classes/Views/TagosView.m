//
//  TagosView.m
//  JWTouch
//
//  Created by Prashannth Vijayakumar on 09/11/17.
//  Copyright © 2017 apple. All rights reserved.
//
#import "TagosView.h"
#import "Constants.h"
#import "JDFTooltips.h"

@interface TagosView()
@property(nonatomic, strong) UILabel *title;
@property(nonatomic, strong) UIView *subview1;
@property(nonatomic, strong) UIView *subview2;
@property(nonatomic, strong) UIImageView *imageView;
@property(nonatomic)BOOL isReversingIntro;
@property(nonatomic)BOOL isShowingIntro;
@property(nonatomic, strong)JDFTooltipView *tooltip;
@end

@implementation TagosView

- (id)initWithContainerView:(UIView *)containerView controlView:(UIView *)controlView{
    CGRect frame = CGRectMake(containerView.frame.size.width, 0, 200, containerView.frame.size.height);

    self = [super initWithFrame:frame];
    if (self) {
        self.isReversingIntro = NO;
        self.isShowingIntro = NO;
        self.containerView = containerView;
        self.controlView = controlView;
        
        self.backgroundColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.5];
//        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds
//                                                       byRoundingCorners:UIRectCornerAllCorners
//                                                             cornerRadii:CGSizeMake(CGRectGetWidth(self.frame), CGRectGetHeight(self.frame))];
//        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
//        maskLayer.frame = self.bounds;
//        maskLayer.path = maskPath.CGPath;
//        self.layer.mask = maskLayer;
        [self setClipsToBounds:YES];
        self.autoresizingMask= UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight;
        
        [self.layer setShadowColor:[UIColor blackColor].CGColor];
        [self.layer setShadowOpacity:0.8];
        [self.layer setShadowRadius:3.0];
        [self.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
        
        self.title = [[UILabel alloc] initWithFrame:CGRectMake(15,self.frame.size.height-185,180,30)];
        self.title.text = STRING_INTRO_TITLE;
        self.title.textColor = [UIColor whiteColor];
        self.title.textAlignment = NSTextAlignmentLeft;
        self.title.font = [UIFont fontWithName: self.title.font.fontName size: 14];
        self.title.layer.shadowOffset = CGSizeMake(0.0,-1.0);
        self.title.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;

        self.subview1 = [[UILabel alloc] initWithFrame:CGRectMake(15,self.frame.size.height-160,180,18)];
        self.subview1.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        self.subview1.layer.shadowOffset = CGSizeMake(0.0,-1.0);

        UILabel *subheader1 = [[UILabel alloc] initWithFrame:CGRectMake(0,0,16,16)];
        subheader1.text = @"1";
        subheader1.font = [UIFont fontWithName: self.title.font.fontName size: 11];
        subheader1.textAlignment = NSTextAlignmentCenter;
        subheader1.textColor = [UIColor whiteColor];
        subheader1.layer.borderColor = [UIColor whiteColor].CGColor;
        subheader1.layer.borderWidth = 1.0f;

        UILabel *subtitle1 = [[UILabel alloc] initWithFrame:CGRectMake(22,0,160,18)];
        subtitle1.text = STRING_INTRO_SUBTITLE1;
        subtitle1.font = [UIFont fontWithName: self.title.font.fontName size: 12];
        subtitle1.textAlignment = NSTextAlignmentLeft;
        subtitle1.textColor = [UIColor whiteColor];
        subtitle1.layer.shadowOffset = CGSizeMake(0.0,-1.0);

        self.subview2 = [[UILabel alloc] initWithFrame:CGRectMake(15,self.frame.size.height-140,180,18)];
        self.subview2.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        self.subview2.layer.shadowOffset = CGSizeMake(0.0,-1.0);

        UILabel *subheader2 =[[UILabel alloc] initWithFrame:CGRectMake(0,0,16,16)];
        subheader2.text = @"2";
        subheader2.font = [UIFont fontWithName: self.title.font.fontName size: 11];
        subheader2.textAlignment = NSTextAlignmentCenter;
        subheader2.textColor = [UIColor whiteColor];
        subheader2.layer.borderColor = [UIColor whiteColor].CGColor;
        subheader2.layer.borderWidth = 1.0f;

        UILabel *subtitle2 = [[UILabel alloc] initWithFrame:CGRectMake(22,0,160,18)];
        subtitle2.text = STRING_INTRO_SUBTITLE2;
        subtitle2.font = [UIFont fontWithName: self.title.font.fontName size: 12];
        subtitle2.textAlignment = NSTextAlignmentLeft;
        subtitle2.textColor = [UIColor whiteColor];
        subtitle2.layer.shadowOffset = CGSizeMake(0.0,-1.0);

        NSString *imageName = @"white-arrow.png";
        UIImage *image = [UIImage imageNamed:imageName inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        self.imageView =  [[UIImageView alloc] initWithFrame:CGRectMake(70, self.frame.size.height-120, 55, 50)];
        self.imageView.layer.shadowOffset = CGSizeMake(0.0,-1.0);
        self.imageView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        self.imageView.image = image;

        [self.subview1 addSubview:subheader1];
        [self.subview1 addSubview:subtitle1];
        [self.subview2 addSubview:subheader2];
        [self.subview2 addSubview:subtitle2];

        [self addSubview:self.title];
        [self addSubview:self.subview1];
        [self addSubview:self.subview2];
        [self addSubview:self.imageView];

        [self.containerView addSubview:self];
    }
    return self;
}

-(void)leftToRightIntro{
    CGRect basketTopFrame = self.frame;
    basketTopFrame.origin.x = self.containerView.frame.size.width;
    [UIView animateWithDuration:3 delay:2.0 options:UIViewAnimationOptionCurveEaseIn animations:^{ self.frame = basketTopFrame; } completion:^(BOOL finished){
    }];
}

-(void)rightToLeftIntro {
    CGRect napkinBottomFrame = self.frame;
    napkinBottomFrame.origin.x = self.containerView.frame.size.width-200;
    [UIView animateWithDuration:3 delay:5.0 options: UIViewAnimationOptionCurveEaseOut animations:^{ self.frame = napkinBottomFrame; } completion:^(BOOL finished){
        [self leftToRightIntro];
    }];
}

-(void)showTooltip{
    self.tooltip = [[JDFTooltipView alloc] initWithTargetView:self.controlView hostView:self.containerView tooltipText:STRING_INTRO_TOOLTIP arrowDirection:JDFTooltipViewArrowDirectionUp width:200.0f];
    [self.tooltip show];
    [self performSelector:@selector(hideTooltip) withObject:self afterDelay:4.0 ];
}

-(void)hideTooltip{
    [self.tooltip hideAnimated:YES];
}

- (void)showIntro{
    [self rightToLeftIntro];
}

-(void)hideIntro{
    CGRect basketTopFrame = self.frame;
    basketTopFrame.origin.x = self.containerView.frame.size.width;
    self.frame = basketTopFrame;
}

-(void)layoutSubviews{
    if([self.tooltip superview]) {
        [self.tooltip hideAnimated:NO];
        [self showTooltip];
    }
}

@end
