//
//  TagosTouch.m
//  Charmboard
//
//  Created by Prashannth Vijayakumar on 9/27/17.
//

#import "TagosTouch.h"
#import "Tagging.h"
#import "Charm.h"
#import "Constants.h"

#import "PiwikTracker.h"

@implementation TagosTouch

- (id) init {
    self = [super init];
    if(self){
        self.localjson = NO;
        self.isCurrentVideoTagged = NO;
        self.isFullScreen = NO;
        self.resumeFullScreen = NO;
        self.isAdPlaying = NO;
        self.videoId=@"";
    }
    return self;
}

-(instancetype) initWithController:(UIViewController *)videoViewC videoId:(NSString *) videoId
{
    self = [super init];
    if (self) {
        self.videoId=videoId;
        self.videoViewC=videoViewC;
        [self.tview setUserInteractionEnabled:NO];
        self.ObjectDict = [[NSMutableDictionary alloc] init];
        self.someDict = [[NSMutableDictionary alloc] init];
        self.searchObjectID = [[NSMutableDictionary alloc] init];
        self.loadedVideos = [[NSMutableArray alloc] init];
        self.myCharms = [NSMutableArray array];
        self.charms = [NSMutableArray array];
    [self.tview setClipsToBounds:YES];
    }
    return self;
}

-(void) initOverlay{
    self.ObjectDict = [[NSMutableDictionary alloc] init];
    self.someDict = [[NSMutableDictionary alloc] init];
    self.searchObjectID = [[NSMutableDictionary alloc] init];
    self.myCharms = [NSMutableArray array];
    self.charms = [NSMutableArray array];
    [self loadCharms];
    if(self.loadedVideos.count <= 1){
        [self addTagosui];
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
        tapRecognizer.numberOfTapsRequired=1;
        [self.tview addGestureRecognizer:tapRecognizer];
        tapRecognizer.delegate=self;
        NSString *p_imageName = @"heart_light_gray.png";
        NSString *m_imageName = @"ic_do_not_disturb_white.png";
        NSString *h_imageName = @"heart_light_gray.png";
        //NSLog("%@",self.navigationController!.viewControllers);
        self.p_image=[UIImage imageNamed:p_imageName inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        self.m_image=[UIImage imageNamed:m_imageName inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        self.h_image=[UIImage imageNamed:h_imageName inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    }
    self.localjson=NO;
    [self loadJson];
    [self loadData];
    [self.tview setUserInteractionEnabled:YES];
    
//    [PiwikTracker sharedInstanceWithSiteID:STRING_PIWIK_SITE_ID baseURL:[NSURL URLWithString:STRING_PIWIK_SERVER_URL]];
//    NSString *cvarname=[NSString stringWithFormat:@"Play_%@",self.videoId];
//    NSString *cvalue=[NSString stringWithFormat:@"guid=ios"];
//    [[PiwikTracker sharedInstance] setCustomVariableForIndex:15 name:cvarname value:cvalue scope:ScreenCustomVariableScope];
//    [[PiwikTracker sharedInstance] sendViews:self.videoId,nil];
}

-(void)hideOverlay{
    [self.controlView setHidden:YES];
    [self.imageView setHidden:YES];
    [self.tview setUserInteractionEnabled:NO];
}

-(void)showOverlay{
    [self.controlView setHidden:NO];
    [self.imageView setHidden:NO];
    [self.tview setUserInteractionEnabled:YES];
    
    if(![self.loadedVideos containsObject:self.videoId]){
        [self.loadedVideos insertObject:self.videoId atIndex:0];
        [self initOverlay];
        [self addTagosIntro];
    }else if([self.loadedVideos indexOfObject:self.videoId] != 0){
        [self.loadedVideos removeObject:self.videoId];
        [self.loadedVideos insertObject:self.videoId atIndex:0];
        [self initOverlay];
        [self addTagosIntro];
    }else{
        [self loadCharms];
        self.textView.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.someDict.count];
    }
}

-(void) loadFloatingButton:(CGFloat *)xCorr withYCorr:(CGFloat *)yCorr withText:(int)text{
    UIButton *floatingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    floatingButton.frame = CGRectMake((*xCorr-20), (*yCorr-20), 40.0, 40.0);
    floatingButton.backgroundColor = [UIColor colorWithRed:14 green:14 blue:14 alpha:0.5];
    floatingButton.layer.cornerRadius = floatingButton.frame.size.height / 2;
    floatingButton.layer.masksToBounds = YES;
    floatingButton.layer.borderWidth = 0;
    CGFloat endx = 0;
    CGFloat endy = 0;
    if(text==0){
        floatingButton.backgroundColor = [UIColor clearColor];
        [floatingButton setImage:_m_image forState:UIControlStateNormal];
    }else if(text==1){
        [floatingButton setImage:_p_image forState:UIControlStateNormal];
        endx=(self.tview.frame.size.width)-80-(*xCorr);
        endy=(self.tview.frame.size.height)-100-(*yCorr);
        [self loadMessage: @"Charm Added"];
    }else{
        [self loadMessage: @"Charm Already"];
        [floatingButton setImage:_h_image forState:UIControlStateNormal];
        endx=(self.tview.frame.size.width)-80-(*xCorr);
        endy=(self.tview.frame.size.height)-100-(*yCorr);
    }
    [floatingButton setTitleColor:[UIColor colorWithWhite: 2 alpha: 0.5] forState: UIControlStateNormal];
    floatingButton.titleLabel.font = [UIFont fontWithName: floatingButton.titleLabel.font.familyName size: 50];
    floatingButton.titleEdgeInsets = UIEdgeInsetsMake(-5, 5, 5, 5);
    [self.tview addSubview:floatingButton];
    
    [UIView animateWithDuration:0.6f delay:0 options: UIViewAnimationOptionTransitionFlipFromTop  animations:^{
            floatingButton.alpha = 0;
            floatingButton.transform = CGAffineTransformMakeTranslation(endx, endy);
        } completion:^(BOOL finished) {
            [floatingButton removeFromSuperview];
    }];
}

-(void) handleTapGesture:(UITapGestureRecognizer *)  gestureRecognizer{
    if(self.isVideoPlaying){
        CGPoint tappedPoint = [gestureRecognizer locationInView: self.tview];
        
        CGFloat xCoordinate = tappedPoint.x * 320/(self.tview.frame.size.width);
        CGFloat yCoordinate = tappedPoint.y * 180/(self.tview.frame.size.height);
        CGFloat xc = tappedPoint.x;
        CGFloat yc = tappedPoint.y;
        
        int time = [self getCurrentTime] * 1000;
        
        if(self.localjson == YES){
            NSLog(@"-----------> LOCAL JSON <-----------");
            int objectid = [self getObject:&xCoordinate withY:&yCoordinate withTime:time];
            NSString *stobjectid = [NSString stringWithFormat:@"obj%d", objectid];
            if(objectid == 0){
                [self loadFloatingButton: &xc withYCorr: &yc withText: 0];
            }else if(self.ObjectDict[stobjectid] != nil){
                Charm *charm = self.ObjectDict[stobjectid];
                NSString *stcharmid = charm.id;
                NSLog(@"Charm ID --> %@",stcharmid);
                
                if(self.someDict[stcharmid] != nil){
                    [self loadFloatingButton: &xc withYCorr: &yc withText: 2];
                }else{
                    NSNumber *newNum = [NSNumber numberWithInt:1];
                    [self.someDict setObject:newNum forKey:stcharmid];
                    self.textView.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.someDict.count];
                    
                    [self.myCharms insertObject:charm atIndex:0];
                    [self touchToFirebase: charm];
                    [self saveCharms];
                    
                    
                    [self loadFloatingButton: &xc withYCorr: &yc withText: 1];
                }
            }else{
                [self loadFloatingButton: &xc withYCorr: &yc withText: 0];
            }
            return;
        }
    
        NSString *urlString = [NSString stringWithFormat:STRING_GET_VIDEO_OBJECT_ID, self.videoId, xCoordinate, yCoordinate, time];
        NSURL *url = [NSURL URLWithString:urlString];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        [[session dataTaskWithRequest:request
                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                        if (error != nil) {
                            NSLog(@"%@", error.localizedDescription);
                        } else{
                            NSString *objectID =[[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                            objectID = [objectID stringByReplacingOccurrencesOfString:@"\""
                                                                           withString:@""];
                            NSLog(@"Tagos objectId=%@", objectID);
                            if([objectID  isEqual: @"0"]){
                                [self floatButton: &xc withYCorr: &yc withText: 0];
                            }else if(self.ObjectDict[objectID] != nil){
                                Charm *charm = self.ObjectDict[objectID];
                                NSString *stcharmid = charm.id;
                                NSLog(@"Charm ID --> %@",stcharmid);
                                if(self.someDict[stcharmid] != nil){
                                   [self floatButton: &xc withYCorr: &yc withText: 2];
                                }else{
                                    NSNumber *newNum = [NSNumber numberWithInt:1];
                                    [self.someDict setObject:newNum forKey:stcharmid];
                                    
                                    [self.myCharms insertObject:charm atIndex:0];
                                    [self saveCharms];
                                    
                                    dispatch_sync(dispatch_get_main_queue(), ^{
                                        self.textView.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.someDict.count];
                                    });
                                    
                                    [self floatButton: &xc withYCorr: &yc withText: 1];
                                }
                            } else {
                                [self floatButton: &xc withYCorr: &yc withText: 0];
                            }
                        }
                        
                    }] resume];
    } else{
        [self hideOverlay];
    }
}

-(void) floatButton:(CGFloat *)xc withYCorr:(CGFloat *)yc withText:(int)text{
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self loadFloatingButton: xc withYCorr: yc withText: text];
    });
}

-(double) getCurrentTime{
    return 0;
}

-(void) loadJson{
    NSString* const API_VIDEO_ID = self.videoId;
    NSString *charmdetails = [NSString stringWithFormat:STRING_GET_JSON, self.videoId];
    NSURL *url = [NSURL URLWithString:charmdetails];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request
                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    if (error != nil) {
                        NSLog(@"%@", error.localizedDescription);
                    } else{
                        @try {
                            NSError *e = nil;
                            NSMutableArray *top = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingAllowFragments error: &e];
                            
                            if (!top) {
                                NSLog(@"Error parsing JSON: %@", e);
                            } else if(self.isCurrentVideoTagged && [API_VIDEO_ID isEqualToString:self.videoId]){
                                self.searchObjectID = [[NSMutableDictionary alloc] init];
                                NSLog(@"Tagging JSON data");
                                NSLog(@"Found --> %lu", (unsigned long)top.count);
                                for(NSArray *tch in top) {
                                    int startX = [[tch objectAtIndex:0] intValue];
                                    int startY = [[tch objectAtIndex:1] intValue];
                                    int endX = [[tch objectAtIndex:2] intValue];
                                    int endY = [[tch objectAtIndex:3] intValue];
                                    int objectID = [[tch objectAtIndex:4] intValue];
                                    
                                    Tagging *tg = [[Tagging alloc] initWithStartX:startX startY:startY endX:endX endY:endY objectID:objectID];
                                    int ss = [[tch objectAtIndex:5] intValue];
                                    int ee = [[tch objectAtIndex:6] intValue];
                                    for(int time = ss; time<=ee; time++){
                                        NSString *sttime = [NSString stringWithFormat:@"%d",time];
                                        if(self.searchObjectID[sttime] != nil){
                                            NSMutableArray<Tagging*> *newArray = self.searchObjectID[sttime];
                                            [newArray addObject:tg];
                                            [self.searchObjectID setObject:newArray forKey:sttime];
                                        }else{
                                            NSMutableArray<Tagging*> *newArray = [[NSMutableArray alloc] init];
                                            [newArray addObject:tg];
                                            [self.searchObjectID setObject:newArray forKey:sttime];
                                        }
                                    }
                                }
                                self.localjson = YES;
                            }
                        }
                        @catch (NSException *exception) {
                            NSLog(@"%@", exception.reason);
                        }
                        @finally {
                        }
                    }
                }] resume];
}

-(void) loadData{
    NSString *video_charms=[NSString stringWithFormat:STRING_GET_TOPCHARMS,self.videoId];
    NSURL *url = [[NSURL alloc] initWithString:video_charms];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request
                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    if (error != nil) {
                        NSLog(@"%@", error.localizedDescription);
                    } else{
                        NSError *error = nil;
                        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                        NSDictionary *top_json = json[@"topcharms"];
                        NSArray *cast = top_json[@"casts"];
                        NSArray *t_charms = cast[0];
                        if ([t_charms isKindOfClass:[NSArray class]]){//Added instrospection as suggested in comment.
                            //NSLog(@"here your charm");
                            for(NSDictionary* charm in t_charms){
                                //charm[@"vid"]=self.videoId;
                                Charm *ch = [[Charm alloc] initWithJSONString:charm];
//                                ch.vid=self.videoId;
                                ch.vid = self.videoId;
                                [self.charms addObject:ch];
                                if(self.ObjectDict[ch.objid] != nil) {
                                    
                                } else {
                                    [self.ObjectDict setObject:ch forKey:ch.objid];
                                }
//                                NSLog(@"Tagos_NumberOfTimeClickOnObject");
//                                NSLog(@"%lu", (unsigned long)self.charms.count);
                            }
                            NSLog(@"Top Charms---> %lu", (unsigned long)self.ObjectDict.count);
                        }
                    }
                    
                }] resume];
}

-(void) isVideoTagged : (void(^)(BOOL ive))completionHandler
{
    self.isCurrentVideoTagged = NO;
    NSString *video_tagged=[NSString stringWithFormat:STRING_GET_VIDEO_TAGGED, self.videoId];
    NSLog(@"%@", video_tagged);
    NSURL *url = [[NSURL alloc] initWithString:video_tagged];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request
                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    if (error != nil) {
                        NSLog(@"%@", error.localizedDescription);
                    } else{
                        NSError *error = nil;
                        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                        int ive = [json[@"ive"] intValue];
                        NSLog(@"Is Video Tagged --> %d",ive);
                        if(ive == 1){
                            self.isCurrentVideoTagged = YES;
                            completionHandler(YES);
                        }else{
                            completionHandler(NO);
                        }
                    }
                    
                }] resume];
}

-(int)  getObject:(CGFloat *)x withY:(CGFloat *)y withTime:(int)time{
    int result = 0;
    int ntime = time / 40;
    NSString *stntime = [NSString stringWithFormat:@"%d", ntime];
    
    if(self.searchObjectID[stntime] != nil){
        NSMutableArray<Tagging*> *tg = [[NSMutableArray alloc] init];
        tg = self.searchObjectID[stntime];
        int len = tg.count;
        NSLog(@"Searching size %d",len);
        for(int i = 0; i < tg.count; i++){
            Tagging *ttg = tg[i];
            if(*x > [ttg getStartX] && *x < [ttg getEndX] && *y >[ttg getStartY] && *y < [ttg getEndY]){
                return [ttg getObjectID];
            }
        }
    }
    return result;
}

-(void) addTagosIntro{
    [self.introView showTooltip];
    [self.introView showIntro];
}

-(void) addTagosui{
    NSString *controlImageName = @"ellipsis.png";
    UIImage *controlImage = [UIImage imageNamed:controlImageName inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    self.controlView = [[UIImageView alloc] initWithFrame:CGRectMake(20, self.tview.frame.size.height-90, 30, 30)];
    self.controlView.image = controlImage;
    
    UITapGestureRecognizer *controlTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(controlTapDetected:)];
    controlTap.numberOfTapsRequired = 1;
    [self.controlView setUserInteractionEnabled:YES];
    [self.controlView addGestureRecognizer:controlTap];
    self.controlView.autoresizingMask = (UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleRightMargin);
    
    NSString *imageName = @"star_test.png";
    UIImage *image = [UIImage imageNamed:imageName inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    self.imageView =  [[UIImageView alloc] initWithFrame:CGRectMake(self.tview.frame.size.width-50, self.tview.frame.size.height-100, 40, 40)];
    self.imageView.image = image;
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
    controlTap.numberOfTapsRequired = 1;
    [self.imageView setUserInteractionEnabled:YES];
    [self.imageView addGestureRecognizer:singleTap];
    self.imageView.autoresizingMask = (UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin);

    NSString *cir_imageName = @"white_circle.png";
    UIImage *cir_image = [UIImage imageNamed:cir_imageName inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    UIImageView *cir_imageView = [[UIImageView alloc] initWithFrame:CGRectMake(-5,-5,20,20)];
    cir_imageView.image = cir_image;
    [self.imageView addSubview:cir_imageView];
    self.textView = [[UITextView alloc] initWithFrame:CGRectMake(-2,-4,25,25)];
    self.textView.textColor = [UIColor blackColor];
    self.textView.textAlignment = NSTextAlignmentCenter;
    [self.textView insertText: [NSString stringWithFormat:@"%lu",(unsigned long)self.myCharms.count]];
    self.textView.backgroundColor = [UIColor clearColor];
    self.textView.font = [UIFont fontWithName: self.textView.font.fontName size: 11];
    [cir_imageView addSubview:self.textView];
    //window
    
    [self addSubviewInPlayer:self.imageView];
    [self addSubviewInPlayer:self.controlView];
    
    self.introView = [[TagosView alloc] initWithContainerView:self.tview controlView:self.controlView];
}

-(void) tapDetected:(UITapGestureRecognizer *)  gestureRecognizer{
    [self pause];
    CharmsViewController *vc = [[CharmsViewController alloc] init];
    vc.video_id = self.videoId;
    vc.yourCharm = self.myCharms;
    vc.delegate = self;
    NSLog(@"Opening board with %lu charms", (unsigned long)self.myCharms.count);
    [self.videoViewC presentViewController:vc animated: YES completion:^{
        if(self.isFullScreen){
            self.resumeFullScreen = YES;
            [self exitFullScreen];
        }
//        [PiwikTracker sharedInstanceWithSiteID:STRING_PIWIK_SITE_ID baseURL:[NSURL URLWithString:STRING_PIWIK_SERVER_URL]];
//        NSString *cvarname=[NSString stringWithFormat:@"Charmboard_%@",self.videoId];
//        NSString *cvalue=[NSString stringWithFormat:@"guid=ios"];
//        [[PiwikTracker sharedInstance] setCustomVariableForIndex:15 name:cvarname value:cvalue scope:ScreenCustomVariableScope];
//        [[PiwikTracker sharedInstance] sendViews:self.videoId,nil];
    }];
}

-(void) addMessage: (NSString *)mtitle withMsubtitle:(NSString *)msubtitle{
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0,self.tview.frame.size.height-90,self.tview.frame.size.width,30)];
    title.text = mtitle;
    title.textColor = [UIColor whiteColor];
    title.textAlignment = NSTextAlignmentCenter;
    title.font = [UIFont fontWithName: title.font.fontName size: 27];
    title.layer.shadowOffset = CGSizeMake(0.0,-1.0);
    
    UILabel *subtitlelable = [[UILabel alloc] initWithFrame:CGRectMake(0,self.tview.frame.size.height-90 + 30,self.tview.frame.size.width,18)];
    subtitlelable.text = msubtitle;
    subtitlelable.font = [UIFont fontWithName: title.font.fontName size: 15];
    subtitlelable.textAlignment = NSTextAlignmentCenter;
    subtitlelable.textColor = [UIColor whiteColor];
    subtitlelable.layer.shadowOffset = CGSizeMake(0.0,-1.0);
    
    [self.tview addSubview:title];
    [self.tview addSubview:subtitlelable];
    [UIView animateWithDuration:2 delay:0 options: UIViewAnimationOptionTransitionFlipFromTop  animations:^{
        title.alpha = 0;
        title.transform = CGAffineTransformMakeTranslation(0, 0);
        subtitlelable.alpha = 0;
        subtitlelable.transform = CGAffineTransformMakeTranslation(0, 0);
    } completion:^(BOOL finished) {
        [title removeFromSuperview];
        [subtitlelable removeFromSuperview];
    }];
}

-(void) addSubviewInPlayer:(UIView *)imageview{
    
}

-(void) loadMessage: (NSString *)msg{
    [self addMessage: msg withMsubtitle: @"on your Charmboard"];
}

-(void) changeTextView{
    self.textView.text = [NSString stringWithFormat:@"%lu",(unsigned long)self.someDict.count];
}

-(void) touchToFirebase: (Charm *)charm{
    
}

-(void) saveCharms{
    NSData *charmsData = [NSKeyedArchiver archivedDataWithRootObject: self.myCharms];
    [[NSUserDefaults standardUserDefaults] setObject:charmsData forKey: @"savedCharms"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"--> Saved < %lu > charms",(unsigned long)[self.myCharms count]);
}

-(void) loadCharms{
    NSData *charmsData = [[NSUserDefaults standardUserDefaults] objectForKey:@"savedCharms"];
    @try {
        self.myCharms = [NSKeyedUnarchiver unarchiveObjectWithData:charmsData];
        NSLog(@"--> Loaded < %lu > charms",(unsigned long)self.myCharms.count);
        self.someDict = [[NSMutableDictionary alloc] init];
        if([self.myCharms count] > 0){
            for(int i=0; i<[self.myCharms count];i++){
                NSNumber *newNum = [NSNumber numberWithInt:1];
                Charm *ch = self.myCharms[i];
                NSString *stcharmid = [NSString stringWithFormat:@"%@", ch.id];
                [self.someDict setObject:newNum forKey:stcharmid];
            }
        } else {
            self.myCharms = [NSMutableArray array];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception.reason);
    }
    @finally {
        
    }
}

-(void) play{
    
}

-(void) pause{
    
}

-(void) resume{
    
}

-(void) exitFullScreen{
    
}

-(void) enterFullScreen{
    
}

-(void) controlSwitch{
    
}

-(BOOL) isVideoPlaying{
    return NO;
}

- (void)showOverlayIfVideoTagged
{
    [self isVideoTagged:^(BOOL ive) {
        if(ive && !self.isAdPlaying){
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self showOverlay];
            });
        }
    }];
}

-(void) controlTapDetected:(UITapGestureRecognizer *)  gestureRecognizer
{
    [self controlSwitch];
}

-(void) resumeVideo
{
    if(self.resumeFullScreen){
        self.resumeFullScreen = NO;
        [self enterFullScreen];
    }
    [self showOverlay];
    [self resume];
}

-(void) updateVideoId:(NSString *) videoId
{
    [self hideOverlay];
    [self.introView hideTooltip];
    [self.introView hideIntro];
    self.isCurrentVideoTagged = NO;
    self.localjson = NO;
    self.videoId = videoId;
    [self showOverlayIfVideoTagged];
}

@end
