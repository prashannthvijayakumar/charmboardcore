#import "MXParallaxHeader.h"

@class MXScrollView;

/**
 The delegate of a MXScrollView object may adopt the MXScrollViewDelegate protocol to control subview's scrolling effect.
 */
@protocol MXScrollViewDelegate <UIScrollViewDelegate>

@optional
/**
 Asks the page if the scrollview should scroll with the subview.
 
 @param scrollView The scrollview. This is the object sending the message.
 @param subView    An instance of a sub view.
 
 @return YES to allow scrollview and subview to scroll together. YES by default.
 */
- (BOOL) scrollView:(nonnull MXScrollView *)scrollView shouldScrollWithSubView:(nonnull UIScrollView *)subView;

@end

/**
 The MXScrollView is a UIScrollView subclass with the ability to hook the vertical scroll from its subviews.
 */
@interface MXScrollView : UIScrollView

/**
 Delegate instance that adopt the MXScrollViewDelegate.
 */
@property (nonatomic, weak, nullable) id<MXScrollViewDelegate> delegate;

@end
