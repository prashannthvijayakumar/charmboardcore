//
//  AllCardTableViewController.m
//  KPCHSdk
//
//  Created by apple on 18/01/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "AllCardTableViewController.h"
#import "Charm.h"
#import "Card.h"
#import "CardView.h"
#import "Constants.h"
#import <SafariServices/SafariServices.h>
#import "PiwikTracker.h"
#import "Haneke.h"
//#import <GoogleAnalytics/GAI.h>
//#import <GoogleAnalytics/GAIDictionaryBuilder.h>
//#import <GoogleAnalytics/GAIFields.h>

@interface AllCardTableViewController () <SFSafariViewControllerDelegate>
@property (nonatomic, strong) NSMutableArray *cards;
@property (nonatomic, assign) NSInteger outfitCount;
@property (nonatomic, strong) UILabel *noDataLabel;
@property (nonatomic, strong) NSIndexPath *lastSeen;
@property (nonatomic, strong) NSMutableArray *currentlyVisible;
@end

@implementation AllCardTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _outfitCount = 0;
    self.lastSeen = [[NSIndexPath alloc] init];
    self.currentlyVisible = [[NSMutableArray alloc] init];
    CGFloat viewWidth = self.view.frame.size.width;
    CGFloat tableRow = viewWidth * 555.0/375.0 ;
    self.tableView.rowHeight=tableRow + 85 ;
    
    _cards = [[NSMutableArray alloc] init];
    if(_cardType != nil){
        if ([_cardType  isEqual: ARRAY_TAB_SECTIONS_HASLOOK_ALL[0]]){
            [self loadData];
            [self loadAccData];
        }else if([_cardType  isEqual: ARRAY_TAB_SECTIONS_HASLOOK_ALL[1]]){
            [self loadData];
        }else if([_cardType  isEqual: ARRAY_TAB_SECTIONS_HASLOOK_ALL[3]]){
            CGFloat tableRow = viewWidth * 680.0/700.0 - 15 ;
            self.tableView.rowHeight=tableRow;
            [self loadLocData];
        }else{
            [self loadAccData];
        }
    }else{
        [self loadAccData];
    }
    _cards = [[NSMutableArray alloc] init];

    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone ;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, self.tableView.bounds.size.height)];
    self.noDataLabel.text             = STRING_NO_CARDS;
    self.noDataLabel.textColor        = [UIColor grayColor];
    self.noDataLabel.textAlignment    = NSTextAlignmentCenter;
    self.noDataLabel.numberOfLines    = 3;
    self.tableView.backgroundView = self.noDataLabel;
}

//-(void) viewWillAppear:(BOOL)animated{
//    NSString *screenName = ARRAY_TAB_SECTIONS_HASLOOK_ALL[2];
//    if(_cardType != nil){
//        if ([_cardType  isEqual: ARRAY_TAB_SECTIONS_HASLOOK_ALL[0]]){
//            screenName = ARRAY_TAB_SECTIONS_HASLOOK_ALL[0];
//        }else if([_cardType  isEqual: ARRAY_TAB_SECTIONS_HASLOOK_ALL[1]]){
//          screenName = ARRAY_TAB_SECTIONS_HASLOOK_ALL[1];
//        }else if([_cardType  isEqual: ARRAY_TAB_SECTIONS_HASLOOK_ALL[3]]){
//            screenName = ARRAY_TAB_SECTIONS_HASLOOK_ALL[3];
//        }
//    }
//    GAI *gai = [GAI sharedInstance];
//    [gai trackerWithTrackingId:STRING_GA_KEY];
//    gai.trackUncaughtExceptions = YES;
////    gai.logger.logLevel = kGAILogLevelVerbose;
//    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
//    [tracker set:kGAIScreenName value:screenName];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(_cards != nil && [_cards count]>0){
        [self.noDataLabel setHidden:YES];
        return [_cards count];
    } else {
        [self.noDataLabel setHidden:NO];
    }
    return 0;
}

- (UIColor *)colorWithHex:(UInt32)col {
    unsigned char r, g, b;
    b = col & 0xFF;
    g = (col >> 8) & 0xFF;
    r = (col >> 16) & 0xFF;
    return [UIColor colorWithRed:(float)r/255.0f green:(float)g/255.0f blue:(float)b/255.0f alpha:1];
}

- (UIColor *)colorWithHexString:(NSString *)str {
    const char *cStr = [str cStringUsingEncoding:NSASCIIStringEncoding];
    long x = strtol(cStr+1, NULL, 16);
    UIColor *ret = [self colorWithHex:x];
    return ret;
}

- (void)tableView:(nonnull UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    self.currentlyVisible = [self.tableView indexPathsForVisibleRows];
    for(int j=0;j<[self.currentlyVisible count];j++){
        NSIndexPath *jPath = (NSIndexPath *)[self.currentlyVisible objectAtIndex:j];
        if(indexPath.row == jPath.row && indexPath.row != self.lastSeen){
            self.lastSeen = indexPath;
            if(self.cards != nil){
                Card *card = _cards[indexPath.row];
                [PiwikTracker sharedInstanceWithSiteID:STRING_PIWIK_SITE_ID baseURL:[NSURL URLWithString:STRING_PIWIK_SERVER_URL]];
                NSString *cvarname=[NSString stringWithFormat:@"cardcampaign"];
                NSString *cvalue=[NSString stringWithFormat:@"cardid=%@|charmid=%@|videoid=%@|cvalue=cysDJd|guid=ios",card.touch_id,self.charm.id,self.charm.vid];
                [[PiwikTracker sharedInstance] setCustomVariableForIndex:14 name:cvarname value:cvalue scope:ScreenCustomVariableScope];
                [[PiwikTracker sharedInstance] sendViews:self.charm.vid, self.charm.id,nil];
            }

        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"cardCell";

    CGFloat cardWidth = self.tableView.frame.size.width;
    
    CGFloat cardHeight = cardWidth * 514.0/375.0 + 100;
    CGFloat imageHeight = cardWidth * 514.0/375.0 + 100;
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, cardWidth-20, 4)];
   
    lineView.backgroundColor = [self colorWithHexString:@"#f4c20d"];
    NSString *image_name = @"ic_launch_white_24dp.png";
    if([_cardType  isEqual: ARRAY_TAB_SECTIONS_HASLOOK_ALL[3]]){
        cardHeight = cardWidth * 514.0/700.0 + 30;
        imageHeight = cardWidth * 514.0/700.0 + 40;
        lineView.backgroundColor = [self colorWithHexString:@"#3cba54" ];
        image_name = @"tp-logo.png";
    }
    CardView *cardView = [[CardView alloc] initWithFrame:CGRectMake(10, 10, cardWidth -20 , cardHeight)];
    cardView.backgroundColor = [UIColor whiteColor];
    UIImageView *imageViewObject = [[UIImageView alloc] initWithFrame:CGRectMake(0, 4, cardWidth - 20, imageHeight-100)];
    imageViewObject.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    imageViewObject.contentMode = UIViewContentModeScaleAspectFill;
    imageViewObject.layer.masksToBounds = false;
    imageViewObject.clipsToBounds = true;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UILabel *subtitlelable= [[UILabel alloc] initWithFrame:CGRectMake(14, imageHeight-90+20, cardWidth-150, 50)];
    subtitlelable.font = [subtitlelable.font fontWithSize:13];
    subtitlelable.textColor = [UIColor grayColor];
    subtitlelable.numberOfLines = 0;
    UILabel *title= [[UILabel alloc] initWithFrame:CGRectMake(14, imageHeight-90, 321, 41)];
    title.font = [subtitlelable.font fontWithSize:17];
    title.textColor = [UIColor grayColor];
    
    //[subtitlelable sizeToFit];
    if([_cardType  isEqual: ARRAY_TAB_SECTIONS_HASLOOK_ALL[3]]){
        [cardView addSubview:title];
    }
    
    UIImage *sh_image = [UIImage imageNamed:image_name inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    UIButton *share_button = [[UIButton alloc] initWithFrame:CGRectMake(cardWidth-120, cardHeight-70, 90, 35)];
    share_button.tag = indexPath.row;
    //share_button.addTarget(self, action: #selector(MXTableViewController.sharebutton_click(_:)), for: UIControlEvents.touchUpInside)
    [share_button setImage:sh_image forState:UIControlStateNormal];
    //share_button.setImage(sh_image, for: UIControlState())

    if([_cardType  isEqual: ARRAY_TAB_SECTIONS_HASLOOK_ALL[3]]){
        [cardView addSubview:title];
        share_button.frame = CGRectMake(cardWidth-80, cardHeight-60 , 39, 33);
    }else{
        share_button.imageEdgeInsets = UIEdgeInsetsMake(8, 8, 8, 64);
        [share_button setTitle:@"BUY IT" forState:UIControlStateNormal];
        share_button.backgroundColor=[self colorWithHexString:@"#c98702"];
        share_button.titleLabel.font = [share_button.titleLabel.font fontWithSize:15];
        share_button.titleEdgeInsets = UIEdgeInsetsMake(8, -70, 8, 5);
        share_button.layer.cornerRadius = 4;
        
    }
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    [cardView addSubview:imageViewObject];
    [cardView addSubview:subtitlelable];
    [cardView addSubview:lineView];
    [cardView addSubview:share_button];
    [cell addSubview:cardView];

    // Configure the cell...
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    if(self.cards != nil){
        Card *card = _cards[indexPath.row];
//        [PiwikTracker sharedInstanceWithSiteID:STRING_PIWIK_SITE_ID baseURL:[NSURL URLWithString:STRING_PIWIK_SERVER_URL]];
//        NSString *cvarname=[NSString stringWithFormat:@"cardcampaign"];
//        NSString *cvalue=[NSString stringWithFormat:@"cardid=%@|charmid=%@|videoid=%@|cvalue=cysDJd|guid=ios",card.touch_id,self.charm.id,self.charm.vid];
//        [[PiwikTracker sharedInstance] setCustomVariableForIndex:14 name:cvarname value:cvalue scope:ScreenCustomVariableScope];
//        [[PiwikTracker sharedInstance] sendViews:self.charm.vid, self.charm.id,nil];
       
        UIColor *placeholder = [UIColor whiteColor];
        if(card.dominant_color != nil && ![card.dominant_color  isEqual:@""]){
            placeholder=[self colorWithHexString:card.dominant_color];
        }
        CGRect rect = CGRectMake(0, 0, 1, 1);
        UIGraphicsBeginImageContext(rect.size);
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetFillColorWithColor(context,
                                       placeholder.CGColor);
        CGContextFillRect(context, rect);
        imageViewObject.image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        
        cell.textLabel.text = [NSString stringWithFormat:@"Row %@", card.title];
        NSString *imageUrl= [NSString stringWithFormat: STRING_IMAGE_LOOK,card.touch_id];
        if(card.domain != nil){
            subtitlelable.text = [NSString stringWithFormat:@"%@ at %@",card.title,card.domain];
        } else{
            subtitlelable.text = [NSString stringWithFormat:@"%@",card.title];
        }
        if([_cardType  isEqual: ARRAY_TAB_SECTIONS_HASLOOK_ALL[3]]){
            imageUrl= [NSString stringWithFormat: STRING_IMAGE_LOCATION,card.touch_id];
            subtitlelable.text = card.subtitle;
            title.text = card.title;
        }else{
            [subtitlelable sizeToFit];
        }
        [imageViewObject hnk_setImageFromURL:[[NSURL alloc] initWithString:imageUrl]];
    }
    return cell;
}

-(void) loadData{
    NSString *video_charms=[NSString stringWithFormat:STRING_GET_CARD_OUTFITS,self.charm.id];
    NSURL *url = [[NSURL alloc] initWithString:video_charms];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request
                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    if(error ==nil){
                        NSError *error = nil;
                        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                        NSArray *cast = json[@"cards"];
                        NSArray *t_charms = cast[0];
                        if ([t_charms isKindOfClass:[NSArray class]]){//Added instrospection as suggested in comment.
                            for(NSDictionary* charm in t_charms){
                                Card *ch = [[Card alloc] initWithJSONString:charm];
                                [self.cards insertObject:ch atIndex:self.outfitCount];
                                self.outfitCount  = self.outfitCount + 1;
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self.tableView reloadData];
                                });
                                //NSLog(@"%@", [self.yourCharm count]);
                            }
                        }
                    }
                    
                }] resume];
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Card *item = _cards[indexPath.row];
    [PiwikTracker sharedInstanceWithSiteID:STRING_PIWIK_SITE_ID baseURL:[NSURL URLWithString:STRING_PIWIK_SERVER_URL]];
    NSString *cvarname=[NSString stringWithFormat:@"Buy-It_%@",item.touch_id];
    NSString *cvalue=[NSString stringWithFormat:@"cardid=%@|charmid=%@|videoid=%@|cvalue=cysDJd|guid=ios",item.touch_id,self.charm.id,self.charm.vid];
    [[PiwikTracker sharedInstance] setCustomVariableForIndex:15 name:cvarname value:cvalue scope:ScreenCustomVariableScope];
    [[PiwikTracker sharedInstance] sendViews:self.charm.vid, self.charm.id,nil];
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:item.brand] options:@{} completionHandler:nil];
    if(item.brand && item.brand != nil){
        SFSafariViewController *safariVC = [[SFSafariViewController alloc]initWithURL:[NSURL URLWithString:item.brand] entersReaderIfAvailable:NO];
        safariVC.delegate = self;
        [self presentViewController:safariVC animated:NO completion:nil];
    }
    //SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithAddress:item.brand ];
    //webViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    //[self presentViewController:webViewController animated:YES completion:NULL];

}
-(void) loadAccData{
    NSString *video_charms=[NSString stringWithFormat:STRING_GET_CARD_ACCESSORIES,self.charm.id];
    NSURL *url = [[NSURL alloc] initWithString:video_charms];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request
                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    if(error ==nil){
                        NSError *error = nil;
                        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                        NSArray *cast = json[@"cards"];
                        NSArray *t_charms = cast[0];
                        if ([t_charms isKindOfClass:[NSArray class]]){//Added instrospection as suggested in comment.
                            for(NSDictionary* charm in t_charms){
                                Card *ch = [[Card alloc] initWithJSONString:charm];
                                
                                [self.cards addObject:ch];
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self.tableView reloadData];
                                });
                                //NSLog(@"%@", [self.yourCharm count]);
                            }
                        }
                    }
                    
                }] resume];
}
-(void) loadLocData{
    NSString *video_charms=[NSString stringWithFormat:STRING_GET_CARD_LOCATION,self.charm.id];
    NSURL *url = [[NSURL alloc] initWithString:video_charms];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request
                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    if(error ==nil){
                        NSError *error = nil;
                        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                        NSArray *cast = json[@"cards"];
                        if([cast isKindOfClass:[NSArray class]]){
                            NSArray *t_charms = cast[0];
                            if ([t_charms isKindOfClass:[NSArray class]]){//Added instrospection as suggested in comment.
                                for(NSDictionary* charm in t_charms){
                                    Card *ch = [[Card alloc] initWithJSONString:charm];
                                
                                    [self.cards addObject:ch];
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [self.tableView reloadData];
                                    });
                                    //NSLog(@"%@", [self.yourCharm count]);
                                }
                            }
                        }
                    }
                }] resume];
}
-(void)safariViewController:(SFSafariViewController *)controller didCompleteInitialLoad:(BOOL)didLoadSuccessfully {
    // Load finished
}

-(void)safariViewControllerDidFinish:(SFSafariViewController *)controller {
    // Done button pressed
}
// takes 0x123456

@end
