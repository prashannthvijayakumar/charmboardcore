//
//  CardViewController.h
//  KPCHSdk
//
//  Created by apple on 18/01/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Charm.h"

@protocol CardViewDelegate
- (void) goToVideo;
@end

@interface CardViewController : UIViewController
@property (nonatomic, weak) id <CardViewDelegate> delegate;
@property (nonatomic, retain) Charm *charm;
@end
