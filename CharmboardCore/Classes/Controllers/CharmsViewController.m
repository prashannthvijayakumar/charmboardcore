// MXParallaxViewController.m
//
// Copyright (c) 2015 Maxime Epain
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "CharmsViewController.h"
#import "MXSegmentedPager.h"
#import "Charm.h"
#import "CharmsTableViewController.h"
#import "Constants.h"

@interface CharmsViewController () <MXSegmentedPagerDelegate, MXSegmentedPagerDataSource>
    @property (nonatomic, strong) MXSegmentedPager  * segmentedPager;
@end

@implementation CharmsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor blackColor];
    [self.view addSubview:self.segmentedPager];
    //_yourCharm = [[NSMutableArray alloc] init];
    //NSLog(@"video data is here %@", _videoData);
    //[self parseJson:_videoData];
    // Segmented Control customization
    self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    self.segmentedPager.segmentedControl.backgroundColor = [UIColor blackColor];
    self.segmentedPager.segmentedControl.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithWhite:1 alpha:0.7],NSFontAttributeName : [UIFont systemFontOfSize:14]};
    self.segmentedPager.segmentedControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],NSFontAttributeName : [UIFont systemFontOfSize:14]};
    self.segmentedPager.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    self.segmentedPager.segmentedControl.selectionIndicatorColor = [UIColor redColor];

    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown) {
        self.segmentedPager.button.frame = CGRectMake(self.view.frame.size.width - 70, self.view.frame.size.height - 70, 50, 50);
    } else {
        self.segmentedPager.button.frame = CGRectMake(self.view.frame.size.height - 70, self.view.frame.size.width - 70, 50, 50);
    }
    self.segmentedPager.button.backgroundColor = [UIColor redColor];
    //self.segmentedPager.button.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin| UIViewAutoresizingFlexibleTopMargin;
    self.segmentedPager.button.layer.cornerRadius = 25;
    NSString *p_imageName = @"ic_play_arrow_white.png";
    self.segmentedPager.button.titleEdgeInsets = UIEdgeInsetsMake(-10, 0, 0, 0);
    
    self.segmentedPager.button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    UIImage *p_image = [UIImage imageNamed:p_imageName inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    self.segmentedPager.button.alpha = 1;
    [self.segmentedPager.button setImage:p_image forState:UIControlStateNormal];
    //self.segmentedPager.parallaxHeader.contentView.frame = CGRect(x:0 ,y:0, width: 0, height: 0);
    //self.segmentedPager.segmentedControlEdgeInsets = UIEdgeInsetsMake(12, 12, 12, 12);
    
    self.segmentedPager.parallaxHeader.contentView.frame = CGRectMake(0, 0, 0, 0);
    
//    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
//    [[NSNotificationCenter defaultCenter]
//     addObserver:self selector:@selector(orientationChanged:)
//     name:UIDeviceOrientationDidChangeNotification
//     object:[UIDevice currentDevice]];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.segmentedPager.parallaxHeader.contentView.frame = CGRectMake(0, 0, 0, 0);
    self.segmentedPager.segmentedControl.segmentEdgeInset = UIEdgeInsetsMake(25, 5, 0, 5);
    //self.segmentedPager.segmentedControl.title
}
-(void) viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.segmentedPager.parallaxHeader.contentView.frame = CGRectMake(0, 0, 0, 0);
    self.segmentedPager.segmentedControl.segmentEdgeInset = UIEdgeInsetsMake(25, 5, 0, 5);
}
- (void)viewWillLayoutSubviews {
    self.segmentedPager.frame = (CGRect){
        .origin = CGPointZero,
        .size   = self.view.frame.size
    };
    [super viewWillLayoutSubviews];
    self.segmentedPager.parallaxHeader.contentView.frame = CGRectMake(0, 0, 0, 0);
    self.segmentedPager.segmentedControl.segmentEdgeInset = UIEdgeInsetsMake(25, 5, 0, 5);
}

#pragma mark Properties
- (void) goBack{
//    [[NSNotificationCenter defaultCenter]
//     removeObserver:self
//     name:UIDeviceOrientationDidChangeNotification
//     object:[UIDevice currentDevice]];
    
    [self goToVideo];
}

-(void) goToVideo{
    [_delegate resumeVideo];
    self.view.backgroundColor = [UIColor clearColor];
    [self dismissViewControllerAnimated:false completion:nil];
}

- (MXSegmentedPager *)segmentedPager {
    if (!_segmentedPager) {
        
        // Set a segmented pager below the cover
        _segmentedPager = [[MXSegmentedPager alloc] init];
        _segmentedPager.delegate    = self;
        _segmentedPager.dataSource  = self;
    }
    
    _segmentedPager.parallaxHeader.contentView.frame = CGRectMake(0, 0, 0, 0);
    return _segmentedPager;
}

#pragma mark <MXSegmentedPagerDelegate>

- (CGFloat)heightForSegmentedControlInSegmentedPager:(MXSegmentedPager *)segmentedPager {
    return 44.f;
}

- (void)segmentedPager:(MXSegmentedPager *)segmentedPager didSelectViewWithTitle:(NSString *)title {
    //NSLog(@"%@ page selected.", title);
}


#pragma mark <MXSegmentedPagerDataSource>

- (NSInteger)numberOfPagesInSegmentedPager:(MXSegmentedPager *)segmentedPager {
    return 2;
}

- (NSString *)segmentedPager:(MXSegmentedPager *)segmentedPager titleForSectionAtIndex:(NSInteger)index {
    return ARRAY_TAB_SECTIONS_CHARM[index];
}

- (UIView *)segmentedPager:(MXSegmentedPager *)segmentedPager viewForPageAtIndex:(NSInteger)index {
    if(index == 0){
            CharmsTableViewController *all = [[CharmsTableViewController alloc]   init];
            all.charms = self.yourCharm;
            all.delegate = self;
            all.noDataText = STRING_NO_CHARM_ADDED;
            //all.player = self.player;
            [self addChildViewController:all];
            return all.view;
    }else{
        CharmsTableViewController *all = [[CharmsTableViewController alloc]   init];
        all.video_id = self.video_id;
        all.delegate = self;
        all.noDataText = STRING_NO_TOP_CHARM;
        //all.player = self.player;
        [self addChildViewController:all];
        return all.view;
    }
}



-(void) parseJson : (NSString *) videoData{
    //NSLog(@"here1");
    NSData *data = [videoData dataUsingEncoding:NSUTF8StringEncoding];
    //NSLog(@"here2");
    
    NSError *error = nil;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];//response object is your response from server as NSData
    //NSLog(@"here3");

    if (!error && [json isKindOfClass:[NSDictionary class]]){
        //NSLog(@"here if");

        self.video_id = json[@"video_id"];
        //Added instrospection as suggested in comment.
        NSArray *yourStaffDictionaryArray = json[@"yourCharms"];
        if ([yourStaffDictionaryArray isKindOfClass:[NSArray class]]){//Added instrospection as suggested in comment.
            //NSLog(@"here your charm");
            for(NSDictionary* charm in yourStaffDictionaryArray){
                Charm *ch = [[Charm alloc] initWithJSONString:charm];
                [self.yourCharm addObject:ch];
                
                //NSLog(@"size"); 
                //NSLog(@"%@", [self.yourCharm count]);
            }
        }
    }
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (void) orientationChanged:(NSNotification *)note
{
    UIDevice * device = note.object;
    switch(device.orientation)
    {
        case UIDeviceOrientationLandscapeLeft:
            [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger: UIInterfaceOrientationPortrait]
                                        forKey:@"orientation"];
            break;

        case UIDeviceOrientationLandscapeRight:
            [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger: UIInterfaceOrientationPortrait]
                                        forKey:@"orientation"];
            break;

        default:
            break;
    };
}

@end
