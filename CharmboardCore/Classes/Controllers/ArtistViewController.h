//
//  CastViewController.h
//  KPCHSdk
//
//  Created by apple on 19/01/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArtistViewController : UITableViewController

@property (nonatomic, retain) NSString *sketchid;
@property (nonatomic, retain) NSString *charmid;
@property (nonatomic, retain) NSString *cast_title;
@property (nonatomic, retain) NSString *cast_image;
@end
