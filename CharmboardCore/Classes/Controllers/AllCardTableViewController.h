//
//  AllCardTableViewController.h
//  KPCHSdk
//
//  Created by apple on 18/01/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Charm.h"
#import <SafariServices/SafariServices.h>
@interface AllCardTableViewController : UITableViewController

@property (nonatomic, retain) Charm *charm;
@property (nonatomic, retain) NSString *cardType;
- (UIColor *)colorWithHexString:(NSString *) str;
- (UIColor *)colorWithHex:(UInt32)col ;
@end
