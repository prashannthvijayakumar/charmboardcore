//
//  CastViewController.m
//  KPCHSdk
//
//  Created by apple on 19/01/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "ArtistViewController.h"
#import "CardView.h"
#import "Haneke.h"
#import "Constants.h"
//#import <GoogleAnalytics/GAI.h>
//#import <GoogleAnalytics/GAIDictionaryBuilder.h>
//#import <GoogleAnalytics/GAIFields.h>

@interface ArtistViewController ()

@property (nonatomic, strong) NSString *cast_dob;
@property (nonatomic, strong) NSString *cast_pob;
@property (nonatomic, strong) NSString *cast_zodiac;
@property (nonatomic, strong) UILabel *noDataLabel;

@end

@implementation ArtistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CGFloat viewWidth = self.view.frame.size.width;
    [self loadSketchsData];
    CGFloat cardGoodRatio = 300.0/332.0;
    _cast_dob=@"";
    _cast_pob=@"";
    _cast_zodiac=@"";
    
    
    //self.tableView.rowHeight = self.view.frame.width * cardGoodRatio
    CGFloat tableRow = viewWidth * cardGoodRatio;
    self.tableView.rowHeight=tableRow;
    if([self.cast_title isEqualToString:@"Uncredited"]){
        self.tableView.rowHeight=2*tableRow;
    }
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, self.tableView.bounds.size.height)];
    self.noDataLabel.text             = STRING_NO_CARDS;
    self.noDataLabel.textColor        = [UIColor grayColor];
    self.noDataLabel.textAlignment    = NSTextAlignmentCenter;
    self.noDataLabel.numberOfLines    = 3;
    self.tableView.backgroundView = self.noDataLabel;
}

//-(void) viewWillAppear:(BOOL)animated{
//    GAI *gai = [GAI sharedInstance];
//    [gai trackerWithTrackingId:STRING_GA_KEY];
//    gai.trackUncaughtExceptions = YES;
////    gai.logger.logLevel = kGAILogLevelVerbose;
//    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
//    [tracker set:kGAIScreenName value:ARRAY_TAB_SECTIONS_HASLOOK_ALL[4]];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.cast_pob != nil && ![self.cast_pob  isEqual: @""]){
        [self.noDataLabel setHidden:YES];
        return 3;
    }else if([self.cast_title isEqualToString:@"Uncredited"]){
        [self.noDataLabel setHidden:YES];
        return 1;
    }else{
        [self.noDataLabel setHidden:NO];
    }
    return 0;
}

- (UIColor *)colorWithHex:(UInt32)col {
    unsigned char r, g, b;
    b = col & 0xFF;
    g = (col >> 8) & 0xFF;
    r = (col >> 16) & 0xFF;
    return [UIColor colorWithRed:(float)r/255.0f green:(float)g/255.0f blue:(float)b/255.0f alpha:1];
}

- (UIColor *)colorWithHexString:(NSString *)str {
    const char *cStr = [str cStringUsingEncoding:NSASCIIStringEncoding];
    long x = strtol(cStr+1, NULL, 16);
    UIColor *ret = [self colorWithHex:x];
    return ret;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"castCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    CGFloat cardGoodRatio = 300.0/332.0;
    CGFloat cardWidth = self.tableView.frame.size.width - 20;
    CGFloat cardHeight = cardWidth * cardGoodRatio;

    CGFloat imageGoodRatio = (246.0/246.0);
    CGFloat imageWidth = cardWidth;
    CGFloat imageHeight = imageWidth * imageGoodRatio;
    
    CardView *cardView = [[CardView alloc] init];
    if([self.cast_title isEqualToString:@"Uncredited"]){
        [cardView setFrame:CGRectMake(10, 10, cardWidth, 2*cardHeight)];
    }else{
        [cardView setFrame:CGRectMake(10, 10, cardWidth, cardHeight)];
    }
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, cardWidth, 4)];
    lineView.backgroundColor = [self colorWithHexString:@"#3cba54"];
    [cardView addSubview:lineView];
    //cardView.autoresizingMask = [.FlexibleBottomMargin]
    if(indexPath.row == 0){
        //cell.frame.height = cardWidth + 100
        NSString *cast_image=[NSString stringWithFormat:STRING_IMAGE_CAST,self.cast_image];
        UIImageView *imageViewObject = [[UIImageView alloc] initWithFrame:CGRectMake(55, 20, cardWidth - 110, imageHeight-110)];
        imageViewObject.autoresizingMask=UIViewAutoresizingFlexibleWidth;
        imageViewObject.contentMode = UIViewContentModeScaleAspectFill;
        imageViewObject.layer.masksToBounds = false;
        imageViewObject.clipsToBounds = true;
        imageViewObject.contentMode = UIViewContentModeScaleAspectFill;
        imageViewObject.layer.cornerRadius = imageViewObject.frame.size.height/2;
        [imageViewObject hnk_setImageFromURL:[[NSURL alloc] initWithString:cast_image]];
        cardView.backgroundColor=[UIColor whiteColor];
        [cardView addSubview:imageViewObject];

        UILabel *subtitlelable  =   [[UILabel alloc] initWithFrame:CGRectMake(0, imageHeight-75, cardWidth, 18)];
        subtitlelable.textAlignment = NSTextAlignmentCenter;
        subtitlelable.text=self.cast_title;
        subtitlelable.font = [subtitlelable.font fontWithSize:20];
        subtitlelable.textColor = [UIColor grayColor];

        if([self.cast_title isEqualToString:@"Uncredited"]){
            subtitlelable.text=@"Do you know who played";
            
            UILabel *subtitlelable2  =   [[UILabel alloc] initWithFrame:CGRectMake(0, imageHeight-50, cardWidth, 18)];
            subtitlelable2.textAlignment = NSTextAlignmentCenter;
            subtitlelable2.text=@"this character?";
            subtitlelable2.font = [subtitlelable.font fontWithSize:20];
            subtitlelable2.textColor = [UIColor grayColor];
            [cardView addSubview:subtitlelable2];
            
            CGFloat borderWidth = 2;
            
            UITextField *nameTextField = [[UITextField alloc] initWithFrame:CGRectMake(15, imageHeight-10, cardWidth-30, 25)];
            nameTextField.placeholder = @"Name of this Actor";
            nameTextField.font =[subtitlelable.font fontWithSize:18];
            nameTextField.textColor = [UIColor darkGrayColor];
            nameTextField.tag = 101; // Setting tag 101 for name
            CALayer *nameBorder = [CALayer layer];
            nameBorder.borderColor = [UIColor lightGrayColor].CGColor;
            nameBorder.frame = CGRectMake(0, nameTextField.frame.size.height - borderWidth, nameTextField.frame.size.width, nameTextField.frame.size.height);
            nameBorder.borderWidth = borderWidth;
            [nameTextField.layer addSublayer:nameBorder];
            nameTextField.layer.masksToBounds = YES;
            [cardView addSubview:nameTextField];
            
            UITextField *emailTextField = [[UITextField alloc] initWithFrame:CGRectMake(15, imageHeight+35, cardWidth-30, 25)];
            emailTextField.placeholder = @"Your Email";
            emailTextField.font =[subtitlelable.font fontWithSize:18];
            emailTextField.textColor = [UIColor darkGrayColor];
            emailTextField.tag = 102;  // Setting tag 102 for email
            CALayer *emailBorder = [CALayer layer];
            emailBorder.borderColor = [UIColor lightGrayColor].CGColor;
            emailBorder.frame = CGRectMake(0, emailTextField.frame.size.height - borderWidth, emailTextField.frame.size.width, emailTextField.frame.size.height);
            emailBorder.borderWidth = borderWidth;
            [emailTextField.layer addSublayer:emailBorder];
            emailTextField.layer.masksToBounds = YES;
            [cardView addSubview:emailTextField];
            
            UILabel *submitMessage  =   [[UILabel alloc] initWithFrame:CGRectMake(15, imageHeight+70, cardWidth-30, 18)];
            submitMessage.textAlignment = NSTextAlignmentCenter;
            submitMessage.text=@"";
            submitMessage.font = [subtitlelable.font fontWithSize:16];
            submitMessage.textColor = [UIColor redColor];
            submitMessage.tag = 103; // Setting tag 103 for submit message
            [cardView addSubview:submitMessage];

            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button addTarget:self
                       action:@selector(submitUncredited:)
             forControlEvents:UIControlEventTouchUpInside];
            [button setTitle:@"SUBMIT" forState:UIControlStateNormal];
            [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            button.frame = CGRectMake(cardWidth/2 - 50, imageHeight+95, 120, 45);
            button.backgroundColor = [self colorWithHexString:@"#e9e9e9"];
            button.layer.cornerRadius = 5;
            button.layer.borderWidth = 1;
            button.layer.borderColor = [self colorWithHexString:@"#e9e9e9"].CGColor;
            button.layer.shadowOffset = CGSizeMake(5, 5);
            button.layer.shadowOpacity = 0.25;
            button.layer.shadowRadius = 1;
            [cardView addSubview:button];
        }
        [cardView addSubview:subtitlelable];
        [cell addSubview:cardView];
        return cell;
    }
    
    cardView.frame =CGRectMake(10, 10, cardWidth, cardHeight-10);
    [cell setNeedsLayout];
    [cell layoutIfNeeded];

    cell.textLabel.backgroundColor= [UIColor clearColor] ;
    UILabel *subtitlelable = [[UILabel alloc] initWithFrame:CGRectMake(45,35, 235, 18)];
    subtitlelable.text=@"Birth Place";
    subtitlelable.font = [subtitlelable.font fontWithSize:12];
    
    subtitlelable.textColor = [UIColor grayColor];
    [cardView addSubview:subtitlelable];
    UILabel *middle_label  = [[UILabel alloc] initWithFrame:CGRectMake(15,(cardHeight-10)/2-45, cardWidth, (cardHeight-30)/2-50)];
    middle_label.textAlignment = NSTextAlignmentCenter;
    NSArray *pod_array = [self.cast_pob componentsSeparatedByString:@","];
    
    middle_label.text=pod_array[0];
    int ss = cardWidth/5;
    middle_label.font = [middle_label.font fontWithSize:ss];
    
    middle_label.textColor = [UIColor grayColor];
    [cardView addSubview:middle_label];
    
    UILabel *down_label = [[UILabel alloc] initWithFrame:CGRectMake(15,cardHeight-80, 235, 40)];
    
    down_label.text=pod_array[1];
    down_label.font = [middle_label.font fontWithSize:22];
    
    down_label.textColor = [UIColor orangeColor];
    [cardView addSubview:down_label];
    UILabel *downr_label = [[UILabel alloc] initWithFrame:CGRectMake(cardWidth-140,cardHeight-80, 140, 40)];
    
    downr_label.text=@"";
    downr_label.font = [middle_label.font fontWithSize:22];
    
    downr_label.textColor = [UIColor orangeColor];
    [cardView addSubview:downr_label];
    
    UILabel *title =[[UILabel alloc] initWithFrame:CGRectMake(45,5, 321, 41)];
    title.text=self.cast_title;
    
    title.textColor = [UIColor blackColor];
    title.font = [title.font fontWithSize:15];;
    
    NSString *sh_imageName = @"ic_place.png";

    if(indexPath.row == 2){
        subtitlelable.text=@"Age";
        double d_dob=[self.cast_dob doubleValue];
        NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:d_dob];
        NSDate* now = [NSDate date];
        NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                           components:NSCalendarUnitYear
                                           fromDate:date
                                           toDate:now
                                           options:0];
        NSString *age = [NSString stringWithFormat:@"%ld Years", (long)[ageComponents year]];
        middle_label.text=age;
        down_label.text=self.cast_zodiac;
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init] ;
        [formatter setDateFormat:@"dd MMM, yyyy"];
        downr_label.text =[formatter stringFromDate:date] ;

        sh_imageName=@"ic_lightbulb_outline.png";
    }
    [middle_label sizeToFit];

    //let sh_image = UIImage(named: sh_imageName)
    
    UIImage *sh_image = [UIImage imageNamed:sh_imageName inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    sh_image = [sh_image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIButton *share_button = [[UIButton alloc] initWithFrame:CGRectMake(0,5,54, 54)];
    [share_button setImage:sh_image forState:UIControlStateNormal];
    //var imageView = UIImageView(image: sh_image!)
    //imageView.frame = CGRect(x: 10 , y:10 , width: 24, height: 24)
    
    share_button.tintColor = [self colorWithHexString:@"#7e0880" ];
    share_button.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [cardView addSubview:share_button];
    [cardView addSubview:title];
    cardView.backgroundColor = [UIColor whiteColor];
    [cell addSubview:cardView];
    cell.backgroundColor=[UIColor clearColor];
    return cell;
}

-(void)submitUncredited:(UITapGestureRecognizer *)  gestureRecognizer{
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    UITextField *name = (UITextField *)[cell viewWithTag:101];
    UITextField *email = (UITextField *)[cell viewWithTag:102];
    UILabel *msg = (UILabel *)[cell viewWithTag:103];
    msg.text = @"";
    msg.textColor=[UIColor redColor];
    [name resignFirstResponder];
    [email resignFirstResponder];
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];

    if([name.text length]==0||[[name.text stringByReplacingOccurrencesOfString:@" " withString:@""] length]==0){
        msg.text=@"Enter a valid name";
    }else if([email.text length]==0 || [emailTest evaluateWithObject:email.text] == NO){
        msg.text=@"Enter a valid email";
    }else{
        NSString *submit_uncredited=[NSString
                                stringWithFormat:STRING_GET_SUBMIT_UNCREDITED,name.text,email.text,self.charmid];
        NSURL *url = [[NSURL alloc] initWithString:submit_uncredited];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        [[session dataTaskWithRequest:request
                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                        if(error ==nil){
                            NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                            if([[jsonString stringByReplacingOccurrencesOfString:@" " withString:@""] isEqualToString:@"\"1\""]){
                                dispatch_sync(dispatch_get_main_queue(), ^{
                                    msg.textColor = [UIColor greenColor];
                                    msg.text=@"Mail successfully sent";
                                });
                            } else {
                                dispatch_sync(dispatch_get_main_queue(), ^{
                                    msg.text=@"Kindly try again later";
                                });
                            }
                        }else{
                            dispatch_sync(dispatch_get_main_queue(), ^{
                                msg.text=@"Kindly try again later";
                            });
                        }
                        
                    }] resume];
    }
}

-(void)loadSketchsData{
    NSString *video_charms=[NSString stringWithFormat:STRING_GET_CAST,self.sketchid];
    NSURL *url = [[NSURL alloc] initWithString:video_charms];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request
                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    if(error ==nil){
                        NSError *error = nil;
                        NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                        //NSLog(@"sketch");
                        
                        if ([json isKindOfClass:[NSArray class]]){//Added instrospection as suggested in comment.
                            for(NSDictionary* cst in json){
                                NSString *type = cst[@"type"];
                                if([type  isEqual: @"dob"]){
                                    NSArray *pob_array = cst[@"birthplace"];
                                    NSArray *dob_array = cst[@"birthdate"];
                                    self.cast_pob = pob_array[0];
                                    self.cast_dob = dob_array[0];
                                    self.cast_zodiac = cst[@"zodiac"];
                                }
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self.tableView reloadData];
                                });
                                //NSLog(@"%@", [self.yourCharm count]);
                            }
                        }
                    }
                    
                }] resume];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
