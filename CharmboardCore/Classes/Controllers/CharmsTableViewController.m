//
//  CharmsTableViewController.m
//  KPCHSdk
//
//  Created by apple on 20/01/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "CharmsTableViewController.h"
#import "CardViewController.h"
#import "CardView.h"
#import "Haneke.h"
#import "Constants.h"
//#import <GoogleAnalytics/GAI.h>
//#import <GoogleAnalytics/GAIDictionaryBuilder.h>
//#import <GoogleAnalytics/GAIFields.h>

@interface CharmsTableViewController ()
@property (nonatomic, strong) UILabel *noDataLabel;
@property (nonatomic, strong) UIImageView *noDataImage;
@end

@implementation CharmsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if(_video_id !=nil){
        _charms = [[NSMutableArray alloc] init];
        [self loadData];
    }
    CGFloat viewWidth = self.view.frame.size.width;
    CGFloat cardGoodRatio = 300.0/332.0;
    CGFloat tableRow = viewWidth * cardGoodRatio - 15 ;
    self.tableView.rowHeight = tableRow;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 100, 0);
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, self.tableView.bounds.size.height)];
    self.noDataLabel.text             = self.noDataText;
    self.noDataLabel.textColor        = [UIColor grayColor];
    self.noDataLabel.textAlignment    = NSTextAlignmentCenter;
    self.noDataLabel.numberOfLines    = 3;
    self.tableView.backgroundView = self.noDataLabel;
    
    self.noDataImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.tableView.bounds.size.width/2 - 50, self.tableView.bounds.size.height/2 - 150, 100, 100)];
    self.noDataImage.contentMode = UIViewContentModeScaleAspectFill;
    self.noDataImage.image = [UIImage imageNamed:STRING_IMAGE_ERROR inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    [self.tableView.backgroundView addSubview:self.noDataImage];
}

//-(void) viewWillAppear:(BOOL)animated{
//    NSString *screenName = ARRAY_TAB_SECTIONS_CHARM[0];
//    GAI *gai = [GAI sharedInstance];
//    [gai trackerWithTrackingId:STRING_GA_KEY];
//    gai.trackUncaughtExceptions = YES;
////    gai.logger.logLevel = kGAILogLevelVerbose;
//    if([self.noDataText isEqualToString:STRING_NO_TOP_CHARM]){
//        screenName = ARRAY_TAB_SECTIONS_CHARM[1];
//    }
//    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
//    [tracker set:kGAIScreenName value:screenName];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger numOfSections = 0;
    if ([self.charms count] > 0){
        [self.noDataLabel setHidden:YES];
        [self.noDataImage setHidden:YES];
        numOfSections                 = 1;
        self.tableView.backgroundView = nil;
    }
    else{
        [self.noDataLabel setHidden:NO];
        if([self.noDataText isEqualToString:STRING_NO_CHARM_ADDED]){
            [self.noDataImage setHidden:YES];
        }else{
            [self.noDataImage setHidden:NO];
        }
    }
    
    return numOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.charms != nil){
        //NSLog(@"length");
        //NSLog(@"%lu", (unsigned long)[_charms count]);
        return [_charms count];
    }
    else{
        return 0;
    }
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CardViewController *nc = [[CardViewController alloc] init];
    nc.charm = _charms[indexPath.row];
    nc.delegate = self;
    [self presentViewController:nc animated:YES completion:nil];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    CGFloat cardGoodRatio = 300.0/332.0;
    CGFloat cardWidth = self.tableView.frame.size.width - 20;
    CGFloat cardHeight = cardWidth * cardGoodRatio;
    CGFloat imageGoodRatio = 246.0/381.0;
    CGFloat imageWidth = cardWidth;
    CGFloat imageHeight = imageWidth * imageGoodRatio;
    CardView *cardView = [[CardView alloc] initWithFrame:CGRectMake(10, 10, cardWidth, cardHeight-10)];
    cardView.backgroundColor = [UIColor whiteColor];
    UIImageView *imageViewObject = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cardWidth, imageHeight)];
    imageViewObject.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    imageViewObject.layer.masksToBounds = false;
    imageViewObject.clipsToBounds = true;
    imageViewObject.contentMode = UIViewContentModeScaleAspectFill;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UILabel *subtitlelable= [[UILabel alloc] initWithFrame:CGRectMake(14, imageHeight+30, 235, 18)];
    subtitlelable.font = [subtitlelable.font fontWithSize:12];
    subtitlelable.textColor = [UIColor grayColor];
    UILabel *title= [[UILabel alloc] initWithFrame:CGRectMake(14, imageHeight, 321, 41)];
    title.font = [subtitlelable.font fontWithSize:17];
    title.textColor = [UIColor grayColor];
    
    
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSString *sh_imageName = @"ic_share_black_24dp.png";
    UIImage *sh_image = [UIImage imageNamed:sh_imageName inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    UIButton *share_button = [[UIButton alloc] initWithFrame:CGRectMake(cardWidth-100, imageHeight, 54, 54)];
    [share_button setImage:sh_image forState:UIControlStateNormal];
    share_button.imageEdgeInsets = UIEdgeInsetsMake(15,15,15,15) ;
    share_button.tag = indexPath.row;
    share_button.userInteractionEnabled = true;
    share_button.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [share_button addTarget:self
                     action:@selector(sharebutton_click:)
           forControlEvents:UIControlEventTouchUpInside];

    NSString *de_imageName = @"ic_delete.png";
    UIImage *de_image = [[UIImage imageNamed:de_imageName inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate ];
    UIButton *de_button = [UIButton buttonWithType:UIButtonTypeCustom];
    de_button.frame=CGRectMake(cardWidth-50, imageHeight, 54, 54);
    de_button.tintColor =[UIColor blackColor];
    [de_button setImage:de_image forState:UIControlStateNormal];
    de_button.imageEdgeInsets = UIEdgeInsetsMake(15,15,15,15) ;
    de_button.tag = indexPath.row;
    de_button.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [de_button addTarget:self
                     action:@selector(deletebutton_click:)
           forControlEvents:UIControlEventTouchUpInside];

    
    [cardView addSubview:share_button];
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    [cardView addSubview:imageViewObject];
    [cardView addSubview:title];
    [cardView addSubview:subtitlelable];
    if(self.video_id == nil){
        [cardView addSubview:de_button];
    }else{
        share_button.frame = CGRectMake(cardWidth-50, imageHeight, 54, 54);
    }
    [cell addSubview:cardView];
    if(self.charms != nil){
        Charm *charm = _charms[indexPath.row];
        cell.textLabel.text = [NSString stringWithFormat:@"Row %@", charm.title];
        float f_w = ([ [[NSString alloc]initWithFormat:@"%@",charm.lcbox[4]] floatValue]);
        float f_h = ([ [[NSString alloc]initWithFormat:@"%@",charm.lcbox[5]] floatValue]);
        float f_x = ([ [[NSString alloc]initWithFormat:@"%@",charm.lcbox[0]] floatValue]);
        float f_y = ([ [[NSString alloc]initWithFormat:@"%@",charm.lcbox[1]] floatValue]);
        
        NSString *imageUrl= [NSString stringWithFormat: STRING_IMAGE_CHARM_CARD,(int)f_w,(int)f_h,(int)f_x,(int)f_y,(int)f_w,(int)f_h,(int)f_w,charm.id];
        [imageViewObject hnk_setImageFromURL:[[NSURL alloc] initWithString:imageUrl] placeholder:[UIImage imageNamed:STRING_IMAGE_PLACEHOLDER inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil]];
        subtitlelable.text = charm.subtitle;
        title.text = charm.title;


        /*
         let imageView = UIImageView(image: sh_image!)
         imageView.frame = CGRect(x: cardWidth-70 , y:cardHeight-60 , width: 24, height: 24)
         imageView.isUserInteractionEnabled = true
     
         
         let de_imageView = UIImageView(image: de_image!)
         de_imageView.frame = CGRect(x: cardWidth-40 , y:cardHeight-60 , width: 24, height: 24)
         de_imageView.isUserInteractionEnabled = true
         
         if (yourCharmString != nil){
         cardView.addSubview(de_button)
         
         }else{
         share_button.frame = CGRect(x: cardWidth-50 , y:imageHeight  , width: 54, height: 54)
         share_button.imageView?.frame = CGRect(x: cardWidth-40 , y:cardHeight-60 , width: 24, height: 24)
         }
         cardView.addSubview(share_button)
         cardView.addSubview(title)
         cardView.backgroundColor = UIColor.white
         cell.addSubview(cardView)
         cell.backgroundColor=UIColor.clear
         */
        cell.backgroundColor = [UIColor clearColor];
    }else{
        cell.textLabel.text = [NSString stringWithFormat:@"Row"];
    }
    return cell;

}
-(void) deletebutton_click:(UIButton*) sender {
    [self.charms removeObjectAtIndex:sender.tag];
    [self.tableView reloadData];

    NSData *charmsData = [NSKeyedArchiver archivedDataWithRootObject: self.charms];
    [[NSUserDefaults standardUserDefaults] setObject:charmsData forKey: @"savedCharms"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void) sharebutton_click :(UIButton*) sender {
    Charm *item = _charms[sender.tag];
    NSString *name=[[item.title lowercaseString] stringByReplacingOccurrencesOfString:@" " withString:@"-"] ;
    NSString *sh_string = [NSString stringWithFormat:STRING_CHARM_SHARE,item.vid,item.id,name];
    NSArray *activityItems = @[sh_string];
    UIActivityViewController *activityViewControntroller = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityViewControntroller.excludedActivityTypes = @[UIActivityTypePrint];
    [self presentViewController:activityViewControntroller animated:true completion:nil];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void) loadData{
    NSString *video_charms=[NSString stringWithFormat:STRING_GET_TOPCHARMS,self.video_id];
    NSURL *url = [[NSURL alloc] initWithString:video_charms];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request
                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    if(error ==nil){
                        NSError *error = nil;
                        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                        NSDictionary *top_json = json[@"topcharms"];
                        NSArray *cast = top_json[@"casts"];
                        NSArray *t_charms = cast[0];
                        if ([t_charms isKindOfClass:[NSArray class]]){//Added instrospection as suggested in comment.
                            //NSLog(@"here your charm");
                            for(NSDictionary* charm in t_charms){
                                //charm[@"vid"]=self.video_id;
                                Charm *ch = [[Charm alloc] initWithJSONString:charm];
                                ch.vid=self.video_id;
                                [self.charms addObject:ch];
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self.tableView reloadData];
                                });
                                //NSLog(@"%@", [self.yourCharm count]);
                            }
                        }
                    }
                    
                }] resume];
}

-(void) goToVideo{
    [_delegate goToVideo];
}
@end
