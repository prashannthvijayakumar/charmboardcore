//
//  CardViewController.m
//  KPCHSdk
//
//  Created by apple on 18/01/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "CardViewController.h"
#import "MXSegmentedPager.h"
#import "AllCardTableViewController.h"
#import "ArtistViewController.h"
#import "CharmsViewController.h"
#import "BuzzViewController.h"
#import "KIImagePager.h"
#import "Constants.h"

@interface CardViewController () <KIImagePagerDelegate, KIImagePagerDataSource,MXSegmentedPagerDelegate, MXSegmentedPagerDataSource>
@property (nonatomic, strong) MXSegmentedPager  * segmentedPager;

@property (nonatomic, strong) NSString *sketchid;
@property (nonatomic, strong) NSString *cast_image;
@property (nonatomic, strong) KIImagePager *imagePager;
@property (nonatomic, strong) NSMutableArray *imageArray;

@end

@implementation CardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _sketchid = @"";
    _cast_image=@"";

    self.view.backgroundColor = [UIColor clearColor];
    [self loadSketchData];

    if([self.charm.haslook isEqualToString:@"0"]){
        self.imageArray = [[NSMutableArray alloc] init];
        [self.imageArray insertObject:[NSString stringWithFormat:STRING_IMAGE_LEAD_CHARM,self.charm.id] atIndex:0];
        [self.imageArray insertObject:[NSString stringWithFormat:STRING_IMAGE_LEAD_CHARM_SLIDER1,self.charm.vid, self.charm.objid] atIndex:1];
        [self.imageArray insertObject:[NSString stringWithFormat:STRING_IMAGE_LEAD_CHARM_SLIDER2,self.charm.vid, self.charm.objid] atIndex:2];
        [self.imageArray insertObject:[NSString stringWithFormat:STRING_VIDEO_LEAD_CHARM_SLIDER,self.charm.vid, self.charm.objid] atIndex:3];

        self.imagePager = [[KIImagePager alloc] initWithFrame:CGRectMake(0,0,200,170)];
        self.imagePager.pageControl.currentPageIndicatorTintColor = [UIColor lightGrayColor];
        self.imagePager.pageControl.pageIndicatorTintColor = [UIColor blackColor];
        self.imagePager.slideshowTimeInterval = 0;
        self.imagePager.slideshowShouldCallScrollToDelegate = NO;
        self.imagePager.imageCaptionDisabled = YES;
        self.imagePager.imageCounterDisabled = YES;
        [self.imagePager setDelegate:self];
        [self.imagePager setDataSource:self];
        self.segmentedPager.parallaxHeader.view = self.imagePager;
    } else {
        UIImageView *headerView= [[UIImageView alloc] init];
        NSString *url =[NSString stringWithFormat:STRING_IMAGE_LEAD_CHARM,self.charm.id] ;
        dispatch_async(dispatch_get_global_queue(0,0), ^{
            NSData * data = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: url]];
            if ( data == nil )
                return;
            dispatch_async(dispatch_get_main_queue(), ^{
                // WARNING: is the cell still using the same data by this point??
                headerView.image = [UIImage imageWithData: data];
            });
        });
        headerView.contentMode = UIViewContentModeScaleAspectFill;
        self.segmentedPager.parallaxHeader.view = headerView;
    }

    self.segmentedPager.parallaxHeader.mode = MXParallaxHeaderModeFill;
    self.segmentedPager.parallaxHeader.height = 205;
    self.segmentedPager.parallaxHeader.minimumHeight = 55;
    self.segmentedPager.segmentedControl.segmentWidthStyle = HMSegmentedControlSegmentWidthStyleDynamic;
    
    [self.view addSubview:self.segmentedPager];
    self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    self.segmentedPager.segmentedControl.backgroundColor = [UIColor clearColor];
    self.segmentedPager.segmentedControl.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithWhite:1 alpha:0.7]};
    self.segmentedPager.segmentedControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.segmentedPager.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    self.segmentedPager.titleLabel.text = self.charm.title;
    self.segmentedPager.subtitleLabel.text =self.charm.subtitle;
    self.segmentedPager.segmentedControl.selectionIndicatorColor = [UIColor redColor];
    // Do any additional setup after loading the view.
    
    [self.segmentedPager.resumeButton setHidden:NO];
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown) {
        self.segmentedPager.resumeButton.frame = CGRectMake(self.view.frame.size.width - 70, self.view.frame.size.height - 70, 50, 50);
    } else {
        self.segmentedPager.resumeButton.frame = CGRectMake(self.view.frame.size.height - 70, self.view.frame.size.width - 70, 50, 50);
    }
    self.segmentedPager.resumeButton.backgroundColor = [UIColor redColor];
    //self.segmentedPager.button.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin| UIViewAutoresizingFlexibleTopMargin;
    self.segmentedPager.resumeButton.layer.cornerRadius = 25;
    NSString *p_imageName = @"ic_play_arrow_white.png";
    self.segmentedPager.resumeButton.titleEdgeInsets = UIEdgeInsetsMake(-10, 0, 0, 0);
    
    self.segmentedPager.resumeButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    UIImage *p_image = [UIImage imageNamed:p_imageName inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    self.segmentedPager.resumeButton.alpha = 1;
    [self.segmentedPager.resumeButton setImage:p_image forState:UIControlStateNormal];

}


- (void)viewWillLayoutSubviews {
    self.segmentedPager.frame = (CGRect){
        .origin = CGPointZero,
        .size   = self.view.frame.size
    };
    [super viewWillLayoutSubviews];
}

- (MXSegmentedPager *)segmentedPager {
    if (!_segmentedPager) {
        
        // Set a segmented pager below the cover
        _segmentedPager = [[MXSegmentedPager alloc] init];
        _segmentedPager.delegate    = self;
        _segmentedPager.dataSource  = self;
    }
    return _segmentedPager;
}

- (void) goBack{
    [self dismissViewControllerAnimated:true completion:nil];
}

-(void) resumeVideo{
     [self dismissViewControllerAnimated:false completion:nil];
     [_delegate goToVideo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



- (void)segmentedPager:(MXSegmentedPager *)segmentedPager didSelectViewWithTitle:(NSString *)title {
    //NSLog(@"%@ page selected.", title);
}

- (void)segmentedPager:(MXSegmentedPager *)segmentedPager didScrollWithParallaxHeader:(MXParallaxHeader *)parallaxHeader {
    
}

#pragma mark <MXSegmentedPagerDataSource>

- (NSInteger)numberOfPagesInSegmentedPager:(MXSegmentedPager *)segmentedPager {
     if([self.charm.haslook isEqualToString:@"0"]){
         return 6;
     }
    return 2;
}

- (NSString *)segmentedPager:(MXSegmentedPager *)segmentedPager titleForSectionAtIndex:(NSInteger)index {
    if([self.charm.haslook isEqualToString:@"0"]){
        return ARRAY_TAB_SECTIONS_HASLOOK_ALL[index];
    }
    return ARRAY_TAB_SECTIONS_HASLOOK_RESTRICTED[index];
}

- (UIView *)segmentedPager:(MXSegmentedPager *)segmentedPager viewForPageAtIndex:(NSInteger)index {
    if([self.charm.haslook isEqualToString:@"0"]){
        switch (index) {
            case 0:
            {
                AllCardTableViewController *all = [[AllCardTableViewController alloc]   init];
                all.charm = self.charm;
                all.cardType = ARRAY_TAB_SECTIONS_HASLOOK_ALL[0];
                [self addChildViewController:all];
                return all.view;
               break;
            }
            case 1:
            {
                AllCardTableViewController *all = [[AllCardTableViewController alloc]   init];
                all.charm = self.charm;
                all.cardType = ARRAY_TAB_SECTIONS_HASLOOK_ALL[1];
                [self addChildViewController:all];
                return all.view;
                break;
            }
            case 2:
            {
                AllCardTableViewController *all = [[AllCardTableViewController alloc]   init];
                all.charm = self.charm;
                all.cardType = ARRAY_TAB_SECTIONS_HASLOOK_ALL[2];
                [self addChildViewController:all];
                return all.view;
                break;
            }
            case 3:
            {
                AllCardTableViewController *all = [[AllCardTableViewController alloc]   init];
                all.charm = self.charm;
                all.cardType = ARRAY_TAB_SECTIONS_HASLOOK_ALL[3];
                [self addChildViewController:all];
                return all.view;
                break;
            }
            case 4:
            {
                ArtistViewController *all = [[ArtistViewController alloc]   init];
                all.sketchid = self.sketchid;
                all.cast_image = self.cast_image;
                all.cast_title =self.charm.title;
                all.charmid = self.charm.id;
                [self addChildViewController:all];
                return all.view;
                break;
            }
            case 5:
            {
                BuzzViewController *all = [[BuzzViewController alloc]   init];
                all.sketchid = self.sketchid;
                [self addChildViewController:all];
                return all.view;
                break;
            }
            default:
            {
                AllCardTableViewController *all = [[AllCardTableViewController alloc]   init];
                all.charm = self.charm;
                all.cardType = ARRAY_TAB_SECTIONS_HASLOOK_ALL[2];
                [self addChildViewController:all];
                return all.view;
                break;
            }
        }
    }else{
        switch (index) {
            case 0:
                {
                    ArtistViewController *all = [[ArtistViewController alloc]   init];
                    all.sketchid = self.sketchid;
                    all.cast_image = self.cast_image;
                    all.cast_title =self.charm.title;
                    all.charmid = self.charm.id;
                    [self addChildViewController:all];
                    return all.view;
                    break;
                }
            case 1:
                {
                    BuzzViewController *all = [[BuzzViewController alloc]   init];
                    all.sketchid = self.sketchid;
                    [self addChildViewController:all];
                    return all.view;
                    break;
                }
            default:
                {
                    AllCardTableViewController *all = [[AllCardTableViewController alloc]   init];
                    all.charm = self.charm;
                    all.cardType = ARRAY_TAB_SECTIONS_HASLOOK_ALL[2];
                    [self addChildViewController:all];
                    return all.view;
                    break;
                }
        }
    }
}

-(void) loadSketchData{
    NSLog(@"----> Charm ID ---> %@", self.charm.id);
    NSString *video_charms=[NSString stringWithFormat:STRING_GET_SKETCH,self.charm.id];
    NSURL *url = [[NSURL alloc] initWithString:video_charms];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request
                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    if(error ==nil){
                        NSError *error = nil;
                        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                        NSLog(@"sketch");
                        if(error == nil){
                            NSLog(@"%@", json[@"sketchid"]);
                            NSLog(@"%@", json[@"cast_image"]);
                            self.sketchid = json[@"sketchid"];
                            self.cast_image = json[@"cast_image"];
                        }
                    }
                    
                }] resume];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - KIImagePager DataSource
- (NSArray *) arrayWithImages:(KIImagePager*)pager
{
    return self.imageArray;
}

- (UIViewContentMode) contentModeForImage:(NSUInteger)image inPager:(KIImagePager *)pager
{
    return UIViewContentModeScaleAspectFill;
}

- (NSString *) captionForImageAtIndex:(NSInteger)index inPager:(KIImagePager *)pager
{
    return @[
             @"Charm",
             @"Second",
             @"Third",
             @"GIF"
             ][index];
}

@end
