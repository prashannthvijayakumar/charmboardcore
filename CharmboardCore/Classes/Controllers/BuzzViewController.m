//
//  BuzzViewController.m
//  JWTouch
//
//  Created by Prashannth Vijayakumar on 07/11/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import "BuzzViewController.h"
#import "CardView.h"
#import "Buzz.h"
#import "Haneke.h"
#import "Constants.h"
//#import <GoogleAnalytics/GAI.h>
//#import <GoogleAnalytics/GAIDictionaryBuilder.h>
//#import <GoogleAnalytics/GAIFields.h>

@interface BuzzViewController ()
@property (nonatomic, strong) NSMutableArray *buzzSources;
@property (nonatomic, strong) NSMutableArray *buzzArray;
@property (nonatomic, strong) UILabel *noDataLabel;
@end

@implementation BuzzViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.buzzArray = [[NSMutableArray alloc] init];
    self.buzzSources = [[NSMutableArray alloc] init];
    self.buzzSources = @[@"twitter",@"ig", @"fb"];

    CGFloat viewWidth = self.view.frame.size.width;
    CGFloat cardGoodRatio = 300.0/332.0;
    [self loadBuzzData];

    CGFloat tableRow = viewWidth * cardGoodRatio;
    self.tableView.rowHeight=tableRow;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, self.tableView.bounds.size.height)];
    self.noDataLabel.text             = STRING_NO_CARDS;
    self.noDataLabel.textColor        = [UIColor grayColor];
    self.noDataLabel.textAlignment    = NSTextAlignmentCenter;
    self.noDataLabel.numberOfLines    = 3;
    self.tableView.backgroundView = self.noDataLabel;

}

//-(void) viewWillAppear:(BOOL)animated{
//    GAI *gai = [GAI sharedInstance];
//    [gai trackerWithTrackingId:STRING_GA_KEY];
//    gai.trackUncaughtExceptions = YES;
////    gai.logger.logLevel = kGAILogLevelVerbose;
//    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
//    [tracker set:kGAIScreenName value:ARRAY_TAB_SECTIONS_HASLOOK_ALL[5]];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.buzzArray != nil && [self.buzzArray count]>0){
        [self.noDataLabel setHidden:YES];
        return [self.buzzArray count];
    }else{
        [self.noDataLabel setHidden:NO];
    }
    return 0;
}

- (UIColor *)colorWithHex:(UInt32)col {
    unsigned char r, g, b;
    b = col & 0xFF;
    g = (col >> 8) & 0xFF;
    r = (col >> 16) & 0xFF;
    return [UIColor colorWithRed:(float)r/255.0f green:(float)g/255.0f blue:(float)b/255.0f alpha:1];
}

- (UIColor *)colorWithHexString:(NSString *)str {
    const char *cStr = [str cStringUsingEncoding:NSASCIIStringEncoding];
    long x = strtol(cStr+1, NULL, 16);
    UIColor *ret = [self colorWithHex:x];
    return ret;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"castCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    
    Buzz *buzz = self.buzzArray[indexPath.row];
    
    CGFloat cardGoodRatio = 300.0/332.0;
    CGFloat cardWidth = self.tableView.frame.size.width - 20;
    CGFloat cardHeight = cardWidth * cardGoodRatio;
    CardView *cardView = [[CardView alloc] initWithFrame:CGRectMake(10, 10, cardWidth, cardHeight)];
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, cardWidth, 4)];
    lineView.backgroundColor = [self colorWithHexString:@"#3cba54"];
    [cardView addSubview:lineView];
    cardView.frame =CGRectMake(10, 10, cardWidth, cardHeight-10);
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    cell.textLabel.backgroundColor= [UIColor clearColor] ;
 
    //Source : twitter - 101 | ig - 102 | fb - 103 | news - 104
    int contentY=60, contentHeight=cardHeight-80, contentFontSize=18,
    thumbnailY=60, iconImageY=15, titleY=5, timeY=35, twitterImageY=15;

    if([buzz.getThumbnail length] > 0) {
        if([buzz.sourceTag isEqualToString:self.buzzSources[0]]) { //Twitter
            thumbnailY = 0;
            iconImageY = cardHeight-120;
            titleY = cardHeight-130;
            timeY = cardHeight-100;
            twitterImageY = cardHeight-120;
        }

        NSString *thumbnailImageStr=buzz.getThumbnail;
        UIImageView *thumbnailImage = [[UIImageView alloc] initWithFrame:CGRectMake(0,thumbnailY,cardWidth,cardHeight-140)];
        thumbnailImage.autoresizingMask=UIViewAutoresizingFlexibleWidth;
        thumbnailImage.contentMode = UIViewContentModeScaleAspectFill;
        thumbnailImage.layer.masksToBounds = false;
        thumbnailImage.clipsToBounds = true;
        [thumbnailImage hnk_setImageFromURL:[[NSURL alloc] initWithString:thumbnailImageStr]];
        [cardView addSubview:thumbnailImage];
        contentY = cardHeight - 75;
        contentHeight=55;
        contentFontSize = 14;
    }
    
    if([buzz.sourceTag isEqualToString:self.buzzSources[0]]) { //Twitter
        NSString *twitterStr = @"logo_twitter.png";
        UIImage *twitterImage = [UIImage imageNamed:twitterStr inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        twitterImage = [twitterImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        UIImageView *twitterImageView = [[UIImageView alloc] initWithFrame:CGRectMake(cardWidth-40,twitterImageY,30,30)];
        twitterImageView.layer.masksToBounds = false;
        twitterImageView.clipsToBounds = true;
        twitterImageView.layer.cornerRadius = 5;
        twitterImageView.image = twitterImage;
        [cardView addSubview:twitterImageView];
    }
    
    NSString *icon_image=[NSString stringWithFormat:STRING_IMAGE_BUZZ_FAVICON,buzz.icon];
    UIImageView *iconImage = [[UIImageView alloc] initWithFrame:CGRectMake(10,iconImageY,40,40)];
    iconImage.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    iconImage.contentMode = UIViewContentModeScaleAspectFill;
    iconImage.layer.masksToBounds = false;
    iconImage.clipsToBounds = true;
    iconImage.layer.cornerRadius = 5;
    [iconImage hnk_setImageFromURL:[[NSURL alloc] initWithString:icon_image]];
    [cardView addSubview:iconImage];
    
    UILabel *title =[[UILabel alloc] initWithFrame:CGRectMake(60,titleY,311,41)];
    title.text=buzz.getTitle;
    title.textColor = [UIColor blackColor];
    title.font = [title.font fontWithSize:14];
    [cardView addSubview:title];
    
    UILabel *time = [[UILabel alloc] initWithFrame:CGRectMake(60,timeY,235,18)];
    time.text=buzz.getDateTime;
    time.font = [time.font fontWithSize:12];
    time.textColor = [UIColor grayColor];
    [cardView addSubview:time];
    
    UILabel *content = [[UILabel alloc] initWithFrame:CGRectMake(15,contentY,cardWidth - 30,contentHeight)];
    NSAttributedString * attrContent = [[NSAttributedString alloc] initWithData:[buzz.getContent dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    content.attributedText = attrContent;
    content.font = [content.font fontWithSize:contentFontSize];
    content.textColor = [UIColor darkGrayColor];
    content.numberOfLines = 0;
    content.lineBreakMode=NSLineBreakByWordWrapping;
//    [content sizeToFit];
    [cardView addSubview:content];
    
    [cell addSubview:cardView];
    cardView.backgroundColor = [UIColor whiteColor];
    cell.backgroundColor=[UIColor clearColor];
    return cell;
}
-(void) loadBuzzData{
    dispatch_group_t group = dispatch_group_create();
    
    for (NSString *source in self.buzzSources) {
        dispatch_group_enter(group);
        
        NSString *urlString = [NSString stringWithFormat:STRING_GET_BUZZ,self.sketchid,source];
        
        NSURL *url = [[NSURL alloc] initWithString:urlString];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
     
            if(error==nil){
                NSError *error = nil;
                NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            
                if ([json isKindOfClass:[NSArray class]]){//Added instrospection as suggested in comment.
                    for(NSDictionary* buzz in json){
                        Buzz *bz = [[Buzz alloc] initWithJSONString:buzz sourceTag:source];
                        if([bz.getContent length]>0 || [bz.thumbnail length]>0 || ([bz.sourceTag isEqualToString:self.buzzSources[1]] && [bz.link length]>0)){ //Check: Instagram
                            [self.buzzArray addObject:bz];
                        }
                    }
                }
            }
            
            dispatch_group_leave(group);
        }];
        [task resume];
    }
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
        self.buzzArray = [[self.buzzArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]] mutableCopy];
        [self.tableView reloadData];
    });
}

@end
