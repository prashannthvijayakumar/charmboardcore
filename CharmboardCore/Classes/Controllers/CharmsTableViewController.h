//
//  CharmsTableViewController.h
//  KPCHSdk
//
//  Created by apple on 20/01/17.
//  Copyright © 2017 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Charm.h"
#import "CardViewController.h"
@protocol CharmsTableViewDelegate
- (void) goToVideo;
@end

@interface CharmsTableViewController : UITableViewController <CardViewDelegate>
@property (nonatomic, weak) id <CharmsTableViewDelegate> delegate;
@property (nonatomic, retain) NSString *video_id;
@property (nonatomic, retain) NSMutableArray *charms;
@property (nonatomic, strong) NSString *noDataText;
@end
